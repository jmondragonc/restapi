var express = require('express')
var path = require('path')
var logger = require('morgan')
var bodyParser = require('body-parser')
var brand = require('./routes/brand')
var client = require('./routes/client')
var report = require('./routes/report')
var csv = require('./routes/csv')
var client_add = require('./routes/client')
var clients = require('./routes/clients')
// Agendamiento
var clientsSuzuki = require('./routes/Agendamiento/client_suzuki')
var adminsSuzuki = require('./routes/Agendamiento/adminSuzuki')
var horariosSuzuki = require('./routes/Agendamiento/horariosSuzuki')
var horariosAgendamiento = require('./routes/Agendamiento/horarioAgendamiento')
var storesAgendamiento = require('./routes/Agendamiento/storeAgendamiento')
var marcasAgendamiento = require('./routes/Agendamiento/marcaAgendamiento')
/*********** */
var office = require('./routes/office')
var clients_brands = require('./routes/clients_brands')
var offices_brands = require('./routes/offices_brands')
var contact = require('./routes/contact')
var models = require('./routes/models')
var models_brands = require('./routes/models_brand')
var version = require('./routes/version')
var version_brands = require('./routes/version_brand')
var users = require('./routes/users')
var testdrive = require('./routes/testdrive')
var auth = require('./routes/auth')
var vins = require("./routes/CampañaSeguridad/vin")
var flotasLeads = require("./routes/DercoFlotas/leadFlota")
var maquinariasLeads = require('./routes/DercoMaquinaria/leadMaquinaria')
// CampañaWsp
var formWsp = require("./routes/CampañasWsp/form");
var userWsp = require("./routes/CampañasWsp/user");
var deparmentWsp = require("./routes/CampañasWsp/departamento");
// dercoparts
var dercoParts = require("./routes/DercoParts/dercoparts")
var newDercoParts = require("./routes/DercoParts/newDercoparts")
// MAzdaCampaña
var mazdaCampaign = require("./routes/MazdaCampaña/mazda");
var NewMazdaCampaign = require("./routes/MazdaCampaña/newMazdaModel");
// Quiter
var quiter = require("./routes/Quiter/quiter")
/**************/

var contactsSeguridad = require("./routes/CampañaSeguridad/contact")
var mongoose = require('mongoose')
const cors = require('cors')


//mongoose.connect('mongodb://localhost:27017/cotizadorderco', { useNewUrlParser: true })
mongoose.connect('mongodb://derco:...seasons@159.65.38.71:27017/derco', { useNewUrlParser: true })
  .then(() => console.log('connection succesful'))
  .catch((err) => console.error(err))

var app = express()
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ 'extended': 'false' }))
app.use(express.static(path.join(__dirname, 'dist')))
var corsOptions = {
  origin: ['https://www.haval.com.pe', 'https://haval.com.pe', 'https://www.renault.pe', 'https://renault.pe', 'https://cotizadorderco.com', 'https://derco.com.pe', 'https://landing.derco.com.pe', 'https://gestor.derco.com.pe', 'https://www.derco.com.pe', 'https://likeseasons.com', 'https://190.216.179.50', 'http://190.216.179.50', 'http://159.223.135.175', 'http://derco-peru.qa.dercoperu.soho.cl', 'http://mazda-catalogo.qa.dercoperu.soho.cl', 'https://45.186.254.122', 'http://mazda-catalogo.qa.dercoperu.soho.cl/', 'https://xenodochial-williams-d12289.netlify.app', 'http://142.93.54.183', 'https://catalogo.mazda.pe', "http://200.123.28.196", "http://localhost:3000", "http://localhost:8889", "https://localhost:8889", "https://camiones-jac-client.qa.dercoperu.soho.cl/", "https://20.232.25.91", "https://10.30.37.12", "https://clientesinternos.pe.derco.cl/"],
}
app.options('*', cors(corsOptions));
app.use(cors(corsOptions));
app.use('/clients', clients)
app.use('/login', express.static(path.join(__dirname, 'dist')))
app.use('/register', express.static(path.join(__dirname, 'dist')))
app.use('/show-client/:id', express.static(path.join(__dirname, 'dist')))
app.use('/show-brand/:id', express.static(path.join(__dirname, 'dist')))
app.use('/edit-client/:id', express.static(path.join(__dirname, 'dist')))
app.use('/edit-brand/:id', express.static(path.join(__dirname, 'dist')))
app.use('/add-client', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-brands', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-brands/:brand', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-brands/:brand/page/:page', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-brands/search/:brand/:date1/:date2', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-brands/:brand/client/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-brands/:brand/client/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/clients-brands', clients_brands)
app.use('/offices-brands', offices_brands)
app.use('/office', office)
// CampañaWsp
app.use('/jacWsp/usuarios', userWsp)
app.use('/jacWsp/departamentos', deparmentWsp)
app.use('/jacWsp/forms', formWsp)
// Dercoparts
app.use('/dercoparts', dercoParts)
app.use('/newdercoparts', newDercoParts)
// CmapañaMazda
app.use('/mazdaCampaign', mazdaCampaign)
app.use('/newMazdaCampaign', NewMazdaCampaign)
// Quiter
app.use('/quiter', quiter)
/**************/
app.use('/clients-by-offices/', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-offices/brand/:slug', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-offices/office/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-offices/office/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/clients-by-offices/office/add/:slug', express.static(path.join(__dirname, 'dist')))
app.use('/client', client)
app.use('/clientFlotas', flotasLeads)
app.use('/clientMaquinaria', maquinariasLeads)
// Agendamiento
app.use('/suzukiClient', clientsSuzuki)
app.use('/suzukiHorarios', horariosSuzuki)
app.use('/adminsSuzuki', adminsSuzuki)
app.use('/agendamiento/horarios', horariosAgendamiento)
app.use('/agendamiento/stores', storesAgendamiento)
app.use('/agendamiento/marcas', marcasAgendamiento)
//************* */
app.use('/models', models)
app.use('/models-brands', models_brands)
app.use("/vins", vins)
app.use("/campaignContact", contactsSeguridad)
app.use('/models/:slug', express.static(path.join(__dirname, 'dist')))
app.use('/models-by-brand/', express.static(path.join(__dirname, 'dist')))
app.use('/models-by-brand/model/:slug', express.static(path.join(__dirname, 'dist')))
app.use('/models-by-brand/model/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/models-by-brand/model/add/:slug', express.static(path.join(__dirname, 'dist')))
app.use('/models-by-brand/model/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/version', version)
app.use('/version-brands', version_brands)
app.use('/versions-by-model/version/:model_id', express.static(path.join(__dirname, 'dist')))
app.use('/versions-by-model/version/add/:slug', express.static(path.join(__dirname, 'dist')))
app.use('/versions-by-model/version/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/versions-by-model/version/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/contact', contact)
app.use('/testdrive', testdrive)
app.use('/clients-list', express.static(path.join(__dirname, 'dist')))
app.use('/brand', brand)
app.use('/brands', express.static(path.join(__dirname, 'dist')))
app.use('/brands/brand/:id', express.static(path.join(__dirname, 'dist')))
app.use('/brands/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/brands/add', express.static(path.join(__dirname, 'dist')))
app.use('/contacts', express.static(path.join(__dirname, 'dist')))
app.use('/contacts/admin', express.static(path.join(__dirname, 'dist')))
app.use('/contacts/admin/:brand', express.static(path.join(__dirname, 'dist')))
app.use('/contacts/admin/:brand/:id', express.static(path.join(__dirname, 'dist')))
app.use('/contacts/admin/edit/:brand/:id', express.static(path.join(__dirname, 'dist')))
app.use('/testdrives', express.static(path.join(__dirname, 'dist')))
app.use('/testdrives/:brand', express.static(path.join(__dirname, 'dist')))
app.use('/testdrives/admin/:brand', express.static(path.join(__dirname, 'dist')))
app.use('/testdrives/admin/:brand/:id', express.static(path.join(__dirname, 'dist')))
app.use('/users-list', express.static(path.join(__dirname, 'dist')))
app.use('/users-list/user/:id', express.static(path.join(__dirname, 'dist')))
app.use('/users-list/user/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/client/supervisor', express.static(path.join(__dirname, 'dist')))
app.use('/client/supervisor/page/:page', express.static(path.join(__dirname, 'dist')))
app.use('/client/supervisor/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/client/supervisor/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/offices/supervisor', express.static(path.join(__dirname, 'dist')))
app.use('/offices/supervisor/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/offices/supervisor/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/contacts/supervisor', express.static(path.join(__dirname, 'dist')))
app.use('/contacts/supervisor/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/contacts/supervisor/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/testdrives/supervisor/add', express.static(path.join(__dirname, 'dist')))
app.use('/testdrives/supervisor/show/:id', express.static(path.join(__dirname, 'dist')))
app.use('/testdrives/supervisor/edit/:id', express.static(path.join(__dirname, 'dist')))
app.use('/client/add', client_add)
app.use('/api/auth', auth)
app.use('/api/users', users)
app.use('/csv', csv)
app.use('/report', report)
app.set('json spaces', 4);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// restful api error handler
app.use(function (err, req, res, next) {
  console.log(err)

  if (req.app.get('env') !== 'development') {
    delete err.stack
  }

  res.status(err.statusCode || 500).json(err)
})

module.exports = app
