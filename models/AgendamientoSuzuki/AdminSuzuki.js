/*eslint-disable */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require("bcrypt-nodejs");

var AdminSuzukiSchema = new Schema(
  {
    first_name: String,
    last_name: String,
    password: {
      type: String,
      required: true
    },
    role: String, // Administrador General, Administrador de Marca, Administrador de Tienda, Vendedor, [Administrador] solo para Suzuki
    email: {
      type: String,
      unique: true,
      required: true
    },
    turn: {
      type: Boolean,
      default: false,
    },
    turnsbyBrand: {
      suzuki: {
        type: Boolean,
        default: false
      },
      mazda: {
        type: Boolean,
        default: false
      },
      haval: {
        type: Boolean,
        default: false
      },
      citroen: {
        type: Boolean,
        default: false
      },  
      jac: {
        type: Boolean,
        default: false
      },
      changan: {
        type: Boolean,
        default: false
      },
      renault: {
        type: Boolean,
        default: false
      },
      greatwall: {
        type: Boolean,
        default: false
      }
    },
    leads: [],
    actived: {
      type: Boolean,
      default: true
    },
    // Campo necesario solo para suzuki, debido que Suzuki no maneja tiendas
    marca: {
      type: String,
      default: null
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

AdminSuzukiSchema.pre("save", function(next) {
  var user = this;
  if (this.isModified("password") || this.isNew) {
    bcrypt.genSalt(10, function(err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, null, function(err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

AdminSuzukiSchema.methods.comparePassword = function(passw, cb) {
  bcrypt.compare(passw, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model("AdminSuzuki", AdminSuzukiSchema);
