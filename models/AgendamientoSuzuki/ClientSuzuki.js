var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ClientSuzukiSchema = new mongoose.Schema({
  names: String,
  phone: String,
  email: String,
  tipo_documento: String,
  numero_documento: String,
  /* marca2: String, */
  model_w2l: String,
  model_final: String,
  date_agenda: String,
  date_filtro: String,
  time: String,
  url1_w2l: String,
  url2_w2l: String,
  terms: {
    type: Boolean,
    default: true
  },
  promotions: {
    type: Boolean,
    default: true
  },
  estado: {
    type: String,
    default: 'Nuevo'
  },
  razon: String,
  clause_info: String,
  clause_prom: String,
  updateFecha: {
    nuevo_date: { type: Date, default: Date.now },
    contactado_date: { type: Date, default: null },
    generado_date: { type: Date, default: null },
    cancelado_date: {type: Date, default: null}
  },
  userid: String,
  sellerName: String,
  store: {
    type: Schema.ObjectId,
    ref: 'StoreAgendamiento',
    default: null
  }
  /* created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now } */
}, {
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('ClientSuzuki', ClientSuzukiSchema)
