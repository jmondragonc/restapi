/*eslint-disable */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var HorarioAgendamientoSchema = new Schema(
  {
    date: {
      type: String,
      required: true
    },
    actived: {
      type: Boolean,
      default: true
    },
    hours: [String],
    marca: {
      type: Schema.ObjectId,
      ref: "MarcaAgendamiento",
      default: null
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("HorarioAgendamientoSchema", HorarioAgendamientoSchema);
