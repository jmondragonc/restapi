/*eslint-disable */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var HorarioSuzukiSchema = new Schema(
  {
    date: {
      type: String,
      required: true,
      unique: true,
    },
    actived: {
      type: Boolean,
      default: true
    },
    hours: [String]
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("HorarioSuzuki", HorarioSuzukiSchema);
