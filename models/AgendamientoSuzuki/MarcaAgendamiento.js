/*eslint-disable */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var MarcaAgendamientoSchema = new Schema(
  {
    name: String,
    slug: String,
    admin: {
      type: Schema.ObjectId,
      ref: "AdminSuzuki",
      default: null
    },
    stores: [
      {
        type: Schema.ObjectId,
        ref: "StoreAgendamiento"
      }
    ]
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("MarcaAgendamiento", MarcaAgendamientoSchema);
