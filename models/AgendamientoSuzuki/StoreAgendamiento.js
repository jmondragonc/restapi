/*eslint-disable */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var StoreAgendamientoSchema = new Schema(
  {
    admin: {
      type: Schema.ObjectId,
      ref: "AdminSuzuki",
      default: null
    },
    society: String,
    local: String,
    direction: String,
    district: String,
    province: String,
    region: String,
    sellers: [
      {
        type: Schema.ObjectId,
        ref: "AdminSuzuki"
      }
    ],
    attention: {
      LV: [String],
      S: [String],
      D: [String]
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("StoreAgendamiento", StoreAgendamientoSchema);
