var mongoose = require('mongoose')

var contactCampaignSchema = new mongoose.Schema(
  {
    placa: String,
    first_names: String,
    last_names: String,
    phone: String,
    email: String,
    marca: String,
    modelo: String,
    anio: String,
    color: String,
    date: String, // yyyy-mm-dd
    hour: String,
    workshop: String,
    vin: String
  },
  {
    versionKey: false,
    timestamps: true
  }
)

module.exports = mongoose.model('contactCampaign', contactCampaignSchema)
