var mongoose = require('mongoose')

var VinSchema = new mongoose.Schema(
  {
    vin: String,
    marca: String,
    modelo: String,
    campana_nombre: String,
    campana_resumen: String,
    campana_descripcion: String,
    status: String
  },
  {
    versionKey: false,
    timestamps: true
  }
)

module.exports = mongoose.model('vin', VinSchema)
