var mongoose = require("mongoose");

var WspDepartamentoSchema = new mongoose.Schema(
  {
    departamento: String,
    locales: [
      {
        local: String,
        ces: String,
        direccion: String,
        orden: {type: Number, default: 0},
        usuarios: [{ type: mongoose.Schema.ObjectId, ref: "wspUser" }]
      }
    ]
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("wspDepartamento", WspDepartamentoSchema);
