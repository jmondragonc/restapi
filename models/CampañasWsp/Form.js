const mongoose = require("mongoose");

const WspForm = new mongoose.Schema(
  {
    nombres: String,
    celular: String,
    correo: String,
    ciudad: String,
    local: {
      type: String,
      default: null
    },
    /* dni: {
      type: String,
      default: null
    },
    termInformativos: {
      type: String,
      default: null
    },
    termPersonales: {
      type: String,
      default: null
    }, */
    vendedor: { type: mongoose.Schema.ObjectId, ref: "wspUser" }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("wspForm", WspForm);
