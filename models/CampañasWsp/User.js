var mongoose = require("mongoose");

var WspUserSchema = new mongoose.Schema(
  {
    cargo: { type: String, default: null },
    nombre: String,
    celular: String,
    correo: String
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("wspUser", WspUserSchema);
