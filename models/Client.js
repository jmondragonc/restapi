var mongoose = require('mongoose')

var ClientSchema = new mongoose.Schema({
  id_w2l: String,
  rut_w2l: String,
  first_name: String,
  last_name: String,
  fone1_w2l: String,
  email: String,
  state: String,
  url1_w2l: String,
  url2_w2l: String,
  brand_w2l: String,
  marca2: String,
  model_w2l: String,
  version_w2l: String,
  sap_w2l: String,
  price_w2l: String,
  local_w2l: String,
  cod_origen2_w2l: String,
  store: String,
  terms: String,
  tipo_documento: String,
  razon_social: String,
  direccion: String,
  distrito: String,
  cc_destino: String,
  vendedor: {
    type: String,
    default: null
  },
  estado: {
    type: String,
    default: 'Nuevo'
  },
  kam: {
    type: String,
    default: null
  },
  observacion: {
    type: String,
    default: null
  },
  seekopId: {
    type: String,
    default: null
  },
  upnifyId: {
    type: String,
    default: null
  },
  prioridad: {
    type: Number,
    default: 0
  },
  salesforce: {
    type: Number,
    default: 0
  },
  financing: {
    type: Object,
    default: null
  },
  retakes: {
    type: Object,
    default: null
  },
  updateFecha: {
    nuevo_date: { type: Date, default: Date.now },
    contactado_date: { type: Date, default: null },
    gestionado_date: { type: Date, default: null },
    cotizado_date: { type: Date, default: null },
    facturado_date: { type: Date, default: null },
    cancelado_date: { type: Date, default: null }
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Client', ClientSchema)
