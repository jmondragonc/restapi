const mongoose = require('mongoose')
const {Schema} = mongoose

const LeadFlota = new Schema({
  names: String,
  tipo_documento: String,
  n_document: String,
  telefono: String,
  email: String,
  provincia: String,
  departamento: {type: String, default: null},
  concesionario: String,
  terminos: String,
  estado: {type: String, default: 'Nuevo'},
  autos: [
    {
      marca: String,
      modelo: String,
      tipo_vehiculo: String,
      flota_solicitar: Number,
      flota_actual: Number
    }
  ],
  comentario: String,
  kam: {type: String, default: null},
  observacion: {type: String, default: null}
},
{
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('leadFlota', LeadFlota)
