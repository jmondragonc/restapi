const mongoose = require("mongoose");

const LeadMaquinaria = new mongoose.Schema(
  {
    nombres: String,
    apellidos: String,
    n_document: String,
    email: String,
    departamento: String,
    t_documento: String,
    estado: { type: String, default: "Nuevo" },
    kam: { type: String, default: null },
    observacion: { type: String, default: null },
    celular: String,
    modelo: String,
    codigo: String,
    terminos: String,
    fecha: String,
    r_social: String
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("leadMaquinaria", LeadMaquinaria);
