const mongoose = require('mongoose')
const {Schema} = mongoose

const LeadDercoParts = new Schema({
  Nombres: String,
  Celular: String,
  Email: String,
  Ciudad: String,
  Vendedor: String,
  Tienda: String,
  Direccion: {type: String, default: null},
  Registro: String
},
{
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('leadDercoParts', LeadDercoParts)
