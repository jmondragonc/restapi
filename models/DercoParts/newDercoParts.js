const mongoose = require('mongoose')
const {Schema} = mongoose

const newLeadDercoParts = new Schema({
  nombre: String,
  telefono: String,
  dni: String,
  email: {type: String, default: null},
  distrito: String,
  vin: String,
  categoria: String,
  repuesto: String,
  brand: {
    type: String,
    default: null
  },
  comentario: {type: String, default: null},
  estado: {type: String, default: 'Nuevo'},
  kam: {type: String, default: null},
  observacion: {type: String, default: null},
  clausula_informativa: {type: Boolean, default: false},
  clausula_datos: {type: String, default: null}
},
{
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('newLeadDercoParts', newLeadDercoParts)
