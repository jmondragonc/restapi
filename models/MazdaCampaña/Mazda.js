const mongoose = require("mongoose");
const { Schema } = mongoose;

const MazdaSchema = new Schema(
  {
    modelo: String,
    img: String,
    versiones: [
      {
        version: String,
        exterior: [
          {
            color: String,
            icono: String,
            imgs: [String]
          }
        ],
        interior:  [
          {
            color: String,
            icono: String,
            imgs: [String]
          }
        ]
      }
    ]
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("mazda", MazdaSchema);
