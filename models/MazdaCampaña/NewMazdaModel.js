const mongoose = require("mongoose");
const { Schema } = mongoose;

const MazdaSchema = new Schema(
  {
    modelo: String,
    img: String,
    versiones: [
      {
        version: String,
        id: String,
        gallery: Object,
      }
    ]
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("NewMazda", MazdaSchema);
