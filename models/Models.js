var mongoose = require('mongoose')

var ModelSchema = new mongoose.Schema({
  name: String,
  brand: String,
  slug: String,
  image: String,
  status: Number,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Model', ModelSchema)
