var mongoose = require('mongoose')

var OfficeSchema = new mongoose.Schema({
  name: String,
  address: String,
  phone: String,
  department: String,
  district: String,
  brand: String,
  slug: String,
  status: Number,
  codigo_sap: String,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Office', OfficeSchema)
