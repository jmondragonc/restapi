const mongoose = require('mongoose')
const {Schema} = mongoose

const FamilyQuiter = new Schema({
  brand: String,
  model: String,
  code: String,
  version: String,
  codigo_sap: String,

  // oldFields
  description: {
    type: String,
    default: null
  },
  slug: {
    type: String,
    default: null
  },
  slug_model: {
    type: String,
    default: null
  }
},
{
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('familyQuiter', FamilyQuiter)
