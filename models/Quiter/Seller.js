const mongoose = require('mongoose')
const {Schema} = mongoose

const SellerQuiter = new Schema({
  id: Number,
  name: String,
  cod: String,
  slug: String
},
{
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('sellerQuiter', SellerQuiter)
