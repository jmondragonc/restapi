const mongoose = require('mongoose')
const {Schema} = mongoose

const SubOriginQuiter = new Schema({
  id: Number,
  name: String,
  cod: String,
  slug: String
},
{
  versionKey: false,
  timestamps: true
})

module.exports = mongoose.model('subOriginQuiter', SubOriginQuiter)
