var mongoose = require('mongoose')

var SeekopSchema = new mongoose.Schema({
  status: {
    type: String,
    default: null
  },
  id: {
    type: String,
    default: null
  },
  message: {
    type: String,
    default: null
  },
  store: {
    type: String,
    default: null
  },
  url1_w2l: {
    type: String,
    default: null
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Seekop', SeekopSchema)
