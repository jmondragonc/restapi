var mongoose = require('mongoose')

var VersionSchema = new mongoose.Schema({
  name: String,
  slug: String,
  brand: String,
  slug_brand: String,
  model: String,
  model_id: String,
  codigo_sap: String,
  status: Number,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Version', VersionSchema)
