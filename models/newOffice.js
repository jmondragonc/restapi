var mongoose = require('mongoose')

var NewOfficeSchema = new mongoose.Schema({
  name: String,
  address: String,
  phone: String,
  department: String,
  district: String,
  brand: String,
  slug: String,
  status: Number,
  codigo_sap: String,
  brands: [],
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model('NewOffice', NewOfficeSchema)
