/*eslint-disable */

var express = require("express");
var router = express.Router();
var ClientSuzuki = require("../../models/AgendamientoSuzuki/ClientSuzuki");
var AdmminSuzuki = require("../../models/AgendamientoSuzuki/AdminSuzuki");
var passport = require("passport");
require("../../config/passport")(passport);

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const admins = await AdmminSuzuki.find();
    return res.json(admins);
  }
);

router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const admin = await AdmminSuzuki.findById(req.params.id);
    const { first_name, last_name, password, role, email, actived, marca } = req.body;
    try {
      if (admin) {
        if (first_name) admin.first_name = first_name;
        if (last_name) admin.last_name = last_name;
        if (password) admin.password = password;
        if (role) admin.role = role;
        if (email) admin.email = email;
        if (marca) admin.marca = marca;
        if (actived != null) admin.actived = actived;
        await admin.save();
        return res.json({ status: "ok", msg: "Se modifico correctamente", data: admin });
      } else {
        return res.json({ status: "error", msg: "No existe admin con este id" });
      }
    } catch (err) {
      return res.json({ status: "error", msg: "Ocurrio un error", error:  err.message });
    }
  }
);

router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    AdmminSuzuki.findByIdAndRemove(req.params.id, async (err, post) => {
      if (err) {
        return res.json({ status: "error", msg: "No se elimino ningun admin", err });
      } else {
        return res.json({
          status: "ok",
          msg: "Se elimino correctamente el admin",
          client: post
        });
      }
    });
  }
);

module.exports = router;
