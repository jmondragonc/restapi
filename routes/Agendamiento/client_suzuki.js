/* eslint-disable spaced-comment */
/* eslint-disable standard/object-curly-even-spacing */
/* eslint-disable camelcase */
var express = require("express");
var router = express.Router();
var ClientAgendamiento = require("../../models/AgendamientoSuzuki/ClientSuzuki");
var AdmminSuzuki = require("../../models/AgendamientoSuzuki/AdminSuzuki");
var StoreAgendamiento = require("../../models/AgendamientoSuzuki/StoreAgendamiento");
var MarcaAgendamiento = require("../../models/AgendamientoSuzuki/MarcaAgendamiento");
var HorarioAgendamiento = require("../../models/AgendamientoSuzuki/HorarioAgendamiento");
var HorarioSuzuki = require("../../models/AgendamientoSuzuki/HorariosSuzuki");
var passport = require("passport");
require("../../config/passport")(passport);

// agendamientos totales solo para Suzuki
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    let leads = await ClientAgendamiento.find({});
    return res.json(leads);
  }
);

// agendamientos totaltes para todas las marcas
router.post(
  "/getAll",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    const { store } = req.body;
    let leads = await ClientAgendamiento.find(
      store ? { store } : { store: null }
    );
    return res.json(leads);
  }
);

router.get(
  // Obtener por userid
  "/:id",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    try {
      const user = await AdmminSuzuki.findById(req.params.id);
      let leads = [];
      if (user) {
        if (user.role === "Administrador") {
          leads = await ClientAgendamiento.find({ store: null });
        } else {
          leads = await ClientAgendamiento.find({ userid: req.params.id });
        }
      } else {
        return res.json({ status: "error", msg: "No hay usuario este id" });
      }
      return res.json(leads);
    } catch (err) {
      return res.json({
        status: "error",
        msg: "Hubo un error al traer los leads",
        err
      });
    }
  }
);

router.post(
  // Obtener por userid
  "/findByAdminId/:id",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    try {
      const user = await AdmminSuzuki.findById(req.params.id);
      const { store } = req.body;
      let leads = [];
      if (user) {
        if (user.role === "Administrador") {
          leads = await ClientAgendamiento.find(
            store ? { store } : { store: null }
          );
        } else {
          leads = await ClientAgendamiento.find(
            store ? { userid: req.params.id, store } : { userid: req.params.id }
          );
        }
      } else {
        return res.json({ status: "error", msg: "No hay usuario este id" });
      }
      return res.json(leads);
    } catch (err) {
      return res.json({
        status: "error",
        msg: "Hubo un error al traer los leads",
        err
      });
    }
  }
);

// store

router.post(
  "/findByDate",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    try {
      const userAdmin = await AdmminSuzuki.findById(req.body.user);
      const { store, date, user } = req.body;
      let clients = [];
      if (userAdmin) {
        if (userAdmin.role === "Administrador") {
          clients = await ClientAgendamiento.find(
            store
              ? { date_filtro: date, store }
              : { date_filtro: date, store: null }
          );
        } else {
          clients = await ClientAgendamiento.find(
            store
              ? { date_filtro: date, userid: user, store }
              : { date_filtro: date, userid: user }
          );
        }
      }

      return res.json(clients);
    } catch (err) {
      return res.json({
        status: "error",
        msg: "Hubo un error al traer los leads",
        err
      });
    }
  }
);

router.post(
  "/findByDateNext/:id",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    try {
      const userAdmin = await AdmminSuzuki.findById(req.params.id);
      let clients = [];
      const { date, store } = req.body;
      let datecurrent = getCurrentDate();
      if (userAdmin) {
        if (userAdmin.role === "Administrador") {
          clients = await ClientAgendamiento.find(
            store
              ? {
                  date_filtro: { $gte: date || datecurrent, $exists: true },
                  store
                }
              : {
                  date_filtro: { $gte: date || datecurrent, $exists: true },
                  store: null
                }
          );
        } else {
          clients = await ClientAgendamiento.find(
            store
              ? {
                  date_filtro: { $gte: date || datecurrent, $exists: true },
                  userid: req.params.id,
                  store
                }
              : {
                  date_filtro: { $gte: date || datecurrent, $exists: true },
                  userid: req.params.id
                }
          );
        }
      }
      return res.json(clients);
    } catch (err) {
      return res.json({
        status: "error",
        msg: "Hubo un error al traer los leads",
        err
      });
    }
  }
);

// Obtener horarios de una fecha

router.post(
  "/horarios",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    const clients = await ClientAgendamiento.find(
      req.body.store
        ? { date_agenda: req.body.date, store: req.body.store }
        : { date_agenda: req.body.date }
    );
    let arrayPost = [];
    if (clients.length > 0) {
      arrayPost = clients.map(c => c.time);
    }
    return res.json({ times: arrayPost });
  }
);
// eslint-disable-next-line standard/object-curly-even-spacing
router.post("/", async (req, res, next) => {
  // post lead
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  // res.setHeader('Access-Control-Allow-Credentials', true);

  try {
    // Verificacion inicial de si existe un agendamiento con el mismo dia y hora
    const clientFound = await ClientAgendamiento.findOne(
      req.body.store
        ? {
            date_agenda: req.body.date_agenda,
            time: req.body.time,
            store: req.body.store
          }
        : { date_agenda: req.body.date_agenda, time: req.body.time }
    );
    if (clientFound) {
      return res.json({
        status: "error",
        msg: "Ya hay un agendamiento con este dia y hora"
      });
    }
    //************************************************************************* */
    let store = await StoreAgendamiento.findById(req.body.store);
    let marca;
    let admins;
    // Si no hay tienda se listan los vendedores de Suzuki
    if (store) {
      marca = await MarcaAgendamiento.findOne({ stores: store });
      admins = await AdmminSuzuki.find({
        _id: { $in: store.sellers },
        actived: true
      });
    } else {
      marca = await MarcaAgendamiento.findOne({ slug: "suzuki" });
      admins = await AdmminSuzuki.find({
        /* role: 'Vendedor', */ actived: true,
        marca: "60883be7334ee50bc8eead10"
      });
    }

    if (admins.length === 0)
      return res.json({
        status: "error",
        msg: "No hay ningun usuario para asignarle un lead"
      });

    let slugBrand = marca.slug == "great-wall" ? "greatwall" : marca.slug;
    let index;

    // set initial turn wiht turnsbyBrand (esta logica aplica para todos los Agendamientos salvo suzuki)
    if (store)
      index = admins.findIndex(c => c.turnsbyBrand[slugBrand] === true);
    // set initial turn wiht turn field (esta logica solo aplica para Agendamiento suzuki)
    else index = admins.findIndex(c => c.turn === true);

    if (index === -1) index = 0;

    let client = new ClientAgendamiento(req.body);
    client.userid = admins[index]._id;
    client.sellerName = `${admins[index].first_name} ${
      admins[index].last_name
    }`;
    await client.save();

    let horarioSuzuki = await HorarioSuzuki.findOne({
      date: req.body.date_agenda
    });
    let horarioAgendamiento = await HorarioAgendamiento.findOne({
      date: req.body.date_agenda
    });

    // Verificar horario
    // Si no hay tienda se traen los horarios de Suzuki
    if (store) {
      if (horarioAgendamiento) {
        if (!horarioAgendamiento.hours.includes(req.body.time)) {
          horarioAgendamiento.hours.push(req.body.time);
        }
      } else {
        horarioAgendamiento = new HorarioAgendamiento({
          date: req.body.date_agenda,
          hours: [req.body.time],
          marca: marca ? marca._id : null
        });
      }
      await horarioAgendamiento.save();
    } else {
      if (horarioSuzuki) {
        if (!horarioSuzuki.hours.includes(req.body.time)) {
          horarioSuzuki.hours.push(req.body.time);
        }
      } else {
        horarioSuzuki = new HorarioSuzuki({
          date: req.body.date_agenda,
          hours: [req.body.time]
        });
      }
      await horarioSuzuki.save();
    }

    // Cambiar turno de admin al cual registrar el agendamiento

    admins[index].turn = false;
    // Si tiene store se trabaja con turnsbyBrand sino (AgendamientoSuzuki) con turn
    if (store) admins[index].turnsbyBrand[slugBrand] = false;
    else admins[index].turn = false;
    //****************** */
    let nextindex = 0;
    if (index !== admins.length - 1) nextindex = index + 1;

    //******************* */
    if (store) admins[nextindex].turnsbyBrand[slugBrand] = true;
    else admins[nextindex].turn = true;
    //****************** */

    await admins[index].save();
    await admins[nextindex].save();
    return res.json({
      status: "ok",
      msg: "Lead Agregado satisfactoriamente",
      lead: client
    });
  } catch (err) {
    console.log(err);
    return res.json({
      status: "error",
      msg: "Ocurrio un error",
      error: err.message
    });
  }
});

router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const { time, date_agenda, model_final } = req.body;
    const client = await ClientAgendamiento.findById(req.params.id);
    try {
      if (client) {
        // const horario = await HorarioSuzuki.findOne({date: date_agenda || client.date_agenda})
        /* if (date_agenda && time) {
        let horarioOld = await HorarioSuzuki.findOne({date: client.date_agenda})
        let i = horarioOld.hours.indexOf(time)
        i !== -1 && horarioOld.hours.splice(i, 1)
        client.date_agenda = date_agenda
        client.time = time
      } */

        if (model_final) client.model_final = model_final;
        await client.save();
        return res.json({
          status: "ok",
          msg: "Se modifico correctamente",
          lead: client
        });
      } else {
        return res.json({ status: "error", msg: "No hay usuario con este id" });
      }
    } catch (err) {
      return res.json({
        status: "error",
        msg: "Ocurrio un error",
        error: err.message
      });
    }
  }
);

router.put(
  "/updateEstado/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const { estado } = req.body;
    const client = await ClientAgendamiento.findById(req.params.id);
    try {
      if (client) {
        switch (estado) {
          case "Nuevo":
            client.estado = estado;
            client.updateFecha.nuevo_date = new Date();
            break;
          case "Contactado":
            client.estado = estado;
            client.updateFecha.contactado_date = new Date();
            break;
          case "Generado":
            client.estado = estado;
            client.updateFecha.generado_date = new Date();
            break;
          case "Cancelado":
            client.estado = estado;
            client.updateFecha.cancelado_date = new Date();
            break;
          default:
            return res.json({
              status: "error",
              msg: "No se modifico ninguna fecha, verifica el estado"
            });
        }
        await client.save();
        return res.json({ status: "ok", msg: "Se modifico correctamente" });
      } else {
        return res.json({
          status: "error",
          msg: "No existe usuario con este id"
        });
      }
    } catch (err) {
      return res.json({
        status: "error",
        msg: "Hubo un error",
        error: err.message
      });
    }
  }
);

router.put("/updateTurns/newTurns", async (req, res) => {
  try {
    let admins = await AdmminSuzuki.find({ role: "Vendedor" });
    admins[0].turn = true;
    await admins[0].save();
    for (let i = 1; i < admins.length; i++) {
      admins[i].turn = false;
      await admins[i].save();
    }
    return res.json({ status: "ok", msg: "Se modifico correctamente" });
  } catch (err) {
    return res.json({
      status: "error",
      msg: "Hubo un error",
      error: err.message
    });
  }
});

router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  async function(req, res, next) {
    ClientAgendamiento.findByIdAndRemove(req.params.id, async (err, post) => {
      if (err) {
        return res.json({
          status: "error",
          msg: "No se elimino ningun cliente",
          err
        });
      } else {
        return res.json({
          status: "ok",
          msg: "Se elimino correctamente",
          client: post
        });
      }
    });
  }
);

const getCurrentDate = () => {
  let d = new Date();
  let month = "" + (d.getMonth() + 1);
  let day = "" + d.getDate();
  let year = d.getFullYear();

  if (month.length < 2) {
    month = "0" + month;
  }
  if (day.length < 2) {
    day = "0" + day;
  }

  return [year, month, day].join("-");
};

const getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
