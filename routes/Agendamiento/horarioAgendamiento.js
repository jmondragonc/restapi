/* eslint-disable spaced-comment */
/* eslint-disable standard/object-curly-even-spacing */
/* eslint-disable camelcase */
var express = require('express')
var router = express.Router()
var HorarioAgendamiento = require('../../models/AgendamientoSuzuki/HorarioAgendamiento')
var passport = require('passport')
require('../../config/passport')(passport)

// horarios

router.get('/', async (req, res) => {
  const horarios = await HorarioAgendamiento.find()
  return res.json(horarios)
})

router.get('/:idMarca', async (req, res) => {
  const horarios = await HorarioAgendamiento.find({marca: req.params.idMarca})
  return res.json(horarios)
})

router.get('/inactived/:idMarca', async (req, res) => {
  const horarios = await HorarioAgendamiento.find({actived: false, marca: req.params.idMarca})
  return res.json(horarios)
})

// Consultar horarios por dia
router.post('/', async (req, res) => {
  const {date, marca} = req.body
  let query = {}
  if (date) query = {...query, date}
  if (marca) query = {...query, marca}
  const horario = await HorarioAgendamiento.findOne(query)
  if (horario) {
    return res.json({status: 'ok', data: horario})
  } else {
    return res.json({ status: 'error', msg: 'No se encontro horario con esta fecha' })
  }
})

router.put('/updateHorario', async (req, res) => {
  const { marca, date, actived, add, remove} = req.body
  let query = {date}
  if (marca) query = {...query, marca}
  const horario = await HorarioAgendamiento.findOne(query)
  try {
    if (horario) {
      if (actived !== null) {
        if (horario.hours.length > 0) {
          horario.actived = actived
        } else {
          return res.json({status: 'error', msg: 'No se puede desactivar esta fecha, existen agendamientos en esta fecha'})
        }
      }
      if (add) {
        add.forEach(a => {
          if (!horario.hours.includes(a)) {
            horario.hours.push(a)
          }
        })
      }
      if (remove) {
        remove.forEach(r => {
          let i = horario.hours.indexOf(r)
          i !== -1 && horario.hours.splice(i, 1)
        })
      }
      await horario.save()
      return res.json({status: 'ok', msg: 'Se modifico correctamente', data: horario})
    } else {
      return res.json({ status: 'error', msg: 'No se encontro horario con esta fecha' })
    }
  } catch (err) {
    return res.json({status: 'error', msg: 'Hubo un error', error: err.message})
  }
})

router.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  HorarioAgendamiento.findByIdAndRemove(req.params.id, async (err, post) => {
    if (err) {
      return res.json({ status: 'error', msg: 'No se elimino ningun cliente', error: err.message })
    } else {
      return res.json({status: 'ok', msg: 'Se elimino correctamente', horario: post})
    }
  })
})

const getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

module.exports = router
