const express = require('express')
const router = express.Router()
const Marca = require('../../models/AgendamientoSuzuki/MarcaAgendamiento')
const passport = require('passport')
require('../../config/passport')(passport)

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    let marcas = await Marca.find()
    return res.json(marcas)
  }
)

router.get(
  '/:slug',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    let marca = await Marca.findOne(
      { slug: req.params.slug },
      { createdAt: 0, updatedAt: 0, admin: 0 }
    ).populate({
      path: 'stores',
      select: '-createdAt -updatedAt -sellers -admin'
    })
    if (marca) return res.json({ status: true, data: marca })
    else return res.json({ status: false, msg: 'No existe marca con este id' })
  }
)

router.get(
  'getById/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    let marca = await Marca.findById(req.params.id)
    if (marca) return res.json({ status: true, data: marca })
    else return res.json({ status: false, msg: 'No existe marca con este id' })
  }
)

router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    Marca.create(req.body, (err, post) => {
      if (err) next(err)
      return res.json({ status: 'Ingresado correctamente', data: post })
    })
  }
)

router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    Marca.findByIdAndUpdate(req.params.id, req.body, (err, post) => {
      if (err) next(err)
      return res.json({ status: 'Modificado correctamente', data: post })
    })
  }
)

router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    Marca.findByIdAndRemove(req.params.id, async (err, post) => {
      if (err) next(err)
      return res.json({ status: 'Eliminado correctamente', data: post })
    })
  }
)

module.exports = router
