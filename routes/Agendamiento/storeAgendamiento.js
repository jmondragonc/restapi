const express = require('express')
const router = express.Router()
const Store = require('../../models/AgendamientoSuzuki/StoreAgendamiento')
const passport = require('passport')
require('../../config/passport')(passport)

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    let stores = await Store.find()
    return res.json(stores)
  }
)

router.post('/', passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    Store.create(req.body, (err, post) => {
      if (err) next(err)
      return res.json({status: 'Ingresado correctamente', data: post})
    })
  })

router.put('/:id', passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    Store.findByIdAndUpdate(req.params.id, req.body, (err, post) => {
      if (err) next(err)
      return res.json({status: 'Modificado correctamente', data: post})
    })
  })

router.delete('/:id', passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    Store.findByIdAndRemove(req.params.id, async (err, post) => {
      if (err) next(err)
      return res.json({status: 'Eliminado correctamente', data: post})
    })
  })

module.exports = router
