var express = require('express')
var router = express.Router()
var ContactModel = require("../../models/CampanaSeguridad/Contact")

router.get("/", async(req, res) => {
    const contacts = await ContactModel.find()
    return res.json(contacts)
})

router.post("/", async(req, res) => {
    try {
        const newVin = await ContactModel.create(req.body)
        return res.json({status: true, data: newVin})
    } catch (err) {
        console.log(err)
        return res.json({status: false, msg: err.message})
    }
})

router.delete("/:id", async (req, res) => {
    ContactModel.findByIdAndRemove(req.params.id, (err, post)=> {
        if (err) return res.json({ status: false, error: err.message });
        else return res.json({ status: true, msg: "Eliminado Correctamente" });
    })
})


module.exports = router;