var express = require('express')
var router = express.Router()
var VinModel = require("../../models/CampanaSeguridad/Vin")

router.get("/", async(req, res) => {
    const vins = await VinModel.find().limit(10)
    return res.json(vins)
})

router.get("/addingByJson", async(req, res) => {
    try {
        await VinModel.deleteMany();
        const jsonArray = require("../../assets/vinsDerco.json")
        await VinModel.insertMany(jsonArray);
        return res.json({status: true, msg: "ok"})
    } catch (err) {
        console.log(err)
        return res.json({status: false, msg: err.message})
    }
})

router.post("/consultVin", async (req, res) => {
    const {vin} = req.body
    const vins = await VinModel.find({vin})
    console.log(vins.length);
    return res.json({status: vins.length !== 0, data: vins})
})




module.exports = router;