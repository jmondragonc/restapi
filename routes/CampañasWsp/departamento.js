const express = require("express");
const router = express.Router();
const departmentModel = require("../../models/CampañasWsp/Departamento");

router.get("/", async (req, res) => {
  const departments = await departmentModel
    .find({})
    .select(
      "-_id -locales.usuarios -locales.direccion -locales._id -locales.orden -createdAt -updatedAt"
    );
  return res.json(departments);
});

router.get("/getAll", async(req, res) => {
  const departments = await departmentModel.find()
  return res.json(departments);
})

router.post("/", async (req, res) => {
  try {
    const newDepartment = new departmentModel(req.body);
    await newDepartment.save();
    return res.json({
      status: true,
      msg: "Guardado exitosamente",
      data: newDepartment
    });
  } catch (err) {
    return res.json({
      status: false,
      error: err.message
    });
  }
});

router.post("/many", async (req, res) => {
  try {
    await departmentModel.create(req.body);
    return res.json({
      status: true,
      msg: "Guardado exitosamente"
    });
  } catch (err) {
    return res.json({
      status: false,
      error: err.message
    });
  }
});

router.delete("/:id", async (req, res) => {
  departmentModel.findByIdAndRemove(req.params.id, (err, post) => {
    if (err) return res.json({ status: false, error: err.message });
    else return res.json({ status: true, msg: "Eliminado correctamente" });
  });
});

module.exports = router;
