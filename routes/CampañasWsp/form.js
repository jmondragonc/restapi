const express = require("express");
const router = express.Router();
var passport = require('passport')
const formModel = require("../../models/CampañasWsp/Form");
const departmentModel = require("../../models/CampañasWsp/Departamento");

require('../../config/passport')(passport)

router.get("/", passport.authenticate('jwt', { session: false}), async (req, res) => {
  const forms = await formModel.find();
  return res.json(forms);
});

router.get("/getAll", async (req, res) => {
  const forms = await formModel.find();
  return res.json(forms);
});

router.post("/", async (req, res) => {
  try {
    const { nombres, celular, correo, ciudad, local } = req.body;
    let vendedor = null;
    if (ciudad) {
      const ciudadFound = await departmentModel.findOne({
        departamento: ciudad
      });
      if (!ciudadFound)
        return res.json({
          status: false,
          error: "Ingrese una ciudad valida"
        });
      let localFound = null;
      localFound = local
        ? ciudadFound.locales.find(c => c.local == local)
        : ciudadFound.locales[0];
      // si en el find es undefined
      localFound = localFound || ciudadFound.locales[0];
      vendedor =
        localFound.usuarios.length > 1
          ? randomElement(localFound.usuarios)
          : localFound.usuarios[0];

      // save
      const newForm = new formModel({
        nombres,
        celular,
        correo,
        ciudad,
        local,
        vendedor
      });
      await newForm.save();
      return res.json({
        status: true,
        msg: "Guardado exitosamente",
        data: newForm
      });
    } else
      return res.json({
        status: false,
        error: "Ingrese una ciudad"
      });
  } catch (err) {
    return res.json({
      status: false,
      error: err.message
    });
  }
});

router.delete("/:id", async (req, res) => {
  formModel.findByIdAndRemove(req.params.id, (err, post) => {
    if (err) return res.json({ status: false, error: err.message });
    else return res.json({ status: true, msg: "Eliminado coprrectamente" });
  });
});

const randomElement = array => array[Math.floor(Math.random() * array.length)];

module.exports = router;
