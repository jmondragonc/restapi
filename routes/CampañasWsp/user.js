const express = require("express");
const router = express.Router();
const userModel = require("../../models/CampañasWsp/User");

router.get("/", async (req, res) => {
  const users = await userModel.find();
  return res.json(users);
});

router.get("/:id", async (req, res) => {
  try {
    const user = await userModel.findById(req.params.id);
    if (user)
      return res.json({
        status: true,
        data: user
      });
    else
      return res.json({
        status: false,
        msg: "No se encontro vendedor con este id"
      });
  } catch (err) {
    return res.json({
      status: false,
      error: err.message
    });
  }
});

router.post("/", async (req, res) => {
  try {
    const newUser = new userModel(req.body);
    await newUser.save();
    return res.json({
      status: true,
      msg: "Guardado exitosamente",
      data: newUser
    });
  } catch (err) {
    return res.json({
      status: false,
      error: err.message
    });
  }
});

router.post("/many", async (req, res) => {
  try {
    await userModel.create(req.body);
    return res.json({
      status: true,
      msg: "Guardado exitosamente"
    });
  } catch (err) {
    return res.json({
      status: false,
      error: err.message
    });
  }
});

router.put("/:id", async (req, res) => {
  const { nombre, celular, correo, cargo } = req.body;
  try {
    const user = await userModel.findById(req.params.id);
    if (user) {
      if (nombre) user.nombre = nombre;
      if (celular) user.celular = celular;
      if (correo) user.correo = correo;
      if (cargo) user.cargo = cargo;
      await user.save();
      return res.json({
        status: true,
        msg: "Modificado Exitosamente"
      });
    } else {
      return res.json({
        status: false,
        msg: "No se encontro usuario con ese id"
      });
    }
  } catch (err) {
    return res.json({
      status: false,
      msg: err.message
    });
  }
});

router.delete("/:id", async (req, res) => {
  userModel.findByIdAndRemove(req.params.id, (err, post) => {
    if (err) return res.json({ status: false, error: err.message });
    else return res.json({ status: true, msg: "Eliminado correctamente" });
  });
});

module.exports = router;
