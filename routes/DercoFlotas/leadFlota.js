const express = require('express')
const router = express.Router()
const LeadFlotaModel = require('../../models/DercoFlotas/LeadFlota')

router.get('/fleets', async (req, res) => {
  try {
    const pageOptions = {
      page: parseInt(req.query.page, 10) || 0,
      limit: parseInt(req.query.limit, 10) || 10
    }
    const leads = await LeadFlotaModel.find().skip(pageOptions.page * pageOptions.limit)
    .limit(pageOptions.limit);
    const config = {
      status: true,
      meta: {
        page: pageOptions.page,
        total: pageOptions.limit,
      },
      data: leads
    }
    return res.json(config);
  } catch (err) {
    return res.json({
      status: false,
      error: err.name,
      msg: err.message
    })
  }
})
router.get('/', async (req, res) => {
  try {
    const leads = await LeadFlotaModel.find();
    return res.json(leads)
  } catch (err) {
    return res.json({
      status: false,
      error: err.name,
      msg: err.message
    })
  }
})

router.post('/filter', async (req, res) => {
  const { date1, date2 } = req.body
  try {
    const leads = await LeadFlotaModel.find({
      createdAt: {
        $gte: new Date(date1 + ' 00:00:00'),
        $lt: new Date(date2 + ' 23:59:59')
      }
    })
    return res.json(leads)
  } catch (err) {
    return res.json({
      status: false,
      error: err.name,
      msg: err.message
    })
  }
})

router.post('/', async (req, res) => {
  try {
    const newLead = new LeadFlotaModel(req.body)
    await newLead.save()
    return res.json({
      status: true,
      msg: 'Guardado exitosamente',
      data: newLead
    })
  } catch (err) {
    return res.json({
      status: false,
      error: err.name,
      msg: err.message
    })
  }
})

router.put('/:id', async (req, res) => {
  try {
    const { kam, observacion, estado } = req.body
    const lead = await LeadFlotaModel.findById(req.params.id)
    if (kam) lead.kam = kam
    if (estado) lead.estado = estado
    if (observacion) lead.observacion = observacion
    await lead.save()
    return res.json({ status: true, msg: 'Modificado Correctamente' })
  } catch (error) {
    return res.json({ status: false, error: error.message })
  }
})

router.delete('/:id', async (req, res) => {
  LeadFlotaModel.findByIdAndRemove(req.params.id, (err, post) => {
    if (err) {
      return res.json({ status: false, name: err.name, error: err.message })
    } else return res.json({ status: true, msg: 'Eliminado Correctamente' })
  })
})

module.exports = router
