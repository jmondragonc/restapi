const express = require('express')
const router = express.Router()
const LeadDercoParts = require('../../models/DercoParts/dercoparts')

router.get('/testInfoFormat', async (req, res) => {
  try {
    const DercoPartsData = require('../../assets/DercoParts.json')
    const DercoPartsDataMap = DercoPartsData.map(({Nombres, Direccion, Registro, ...keepFields}) => {
      return {Nombres: utf8_decode(Nombres), Direccion: formatDirection(Direccion), Registro: processDate(Registro), ...keepFields}
    })
    await LeadDercoParts.create(DercoPartsDataMap)
    return res.json({status: true})
  } catch (err) {
    console.log(err)
    return res.json({prueba: 'Fallo'})
  }
})

router.get('/', async (req, res) => {
  const leadsDercoParts = await LeadDercoParts.find()
  return res.json(leadsDercoParts)
})

router.post('/filter', async (req, res) => {
  const { date1, date2 } = req.body
  try {
    const leads = await LeadDercoParts.find({Registro: {
      $gte: date1,
      $lte: date2
    }})
    return res.json({status: true, data: leads})
  } catch (err) {
    return res.json({
      status: false,
      error: err.name,
      msg: err.message
    })
  }
})

router.post('/', async (req, res, next) => {
  LeadDercoParts.create(req.body, (err, post) => {
    if (err) return res.json({status: false, msg: 'No se guardo el registro', error: err.message})
    return res.json({status: true, data: post})
  })
})

router.delete('/', async (req, res) => {
  LeadDercoParts.remove({}, (err, post) => {
    if (err) return res.json({status: false, msg: 'No se pudo eliminar', error: err.message})
    return res.json({status: true, msg: 'Eliminados correctamente'})
  })
})

module.exports = router

// *************************** Formatring functions ************************************************

const formatDayAndMonth = number => {
  return number.length == 1 ? `0${number}` : number
}

const processDate = date => {
  var parts = date.split(' ')[0].split('/')
  // var newdate = new Date(`20${parts[2]}` + '-' + formatDayAndMonth(parts[0]) + '-' + formatDayAndMonth(parts[1]))
  return `20${parts[2]}` + '-' + formatDayAndMonth(parts[0]) + '-' + formatDayAndMonth(parts[1])
}

const formatDirection = direction => {
  return direction ? direction.split('\t')[0] : null
}

function utf8_decode (strData) {
  const tmpArr = []
  let i = 0
  let c1 = 0
  let seqlen = 0
  strData += ''
  while (i < strData.length) {
    c1 = strData.charCodeAt(i) & 0xFF
    seqlen = 0
    // https://en.wikipedia.org/wiki/UTF-8#Codepage_layout
    if (c1 <= 0xBF) {
      c1 = (c1 & 0x7F)
      seqlen = 1
    } else if (c1 <= 0xDF) {
      c1 = (c1 & 0x1F)
      seqlen = 2
    } else if (c1 <= 0xEF) {
      c1 = (c1 & 0x0F)
      seqlen = 3
    } else {
      c1 = (c1 & 0x07)
      seqlen = 4
    }
    for (let ai = 1; ai < seqlen; ++ai) {
      c1 = ((c1 << 0x06) | (strData.charCodeAt(ai + i) & 0x3F))
    }
    if (seqlen === 4) {
      c1 -= 0x10000
      tmpArr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)))
      tmpArr.push(String.fromCharCode(0xDC00 | (c1 & 0x3FF)))
    } else {
      tmpArr.push(String.fromCharCode(c1))
    }
    i += seqlen
  }
  return tmpArr.join('')
}

module.exports = router
