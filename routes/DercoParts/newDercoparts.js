const express = require('express')
const router = express.Router()
const LeadDercoParts = require('../../models/DercoParts/newDercoParts')

router.get('/', async (req, res) => {
  const leadsDercoParts = await LeadDercoParts.find()
  return res.json(leadsDercoParts)
})

router.post('/filter', async (req, res) => {
  const { date1, date2 } = req.body
  try {
    const leads = await LeadDercoParts.find({createdAt: {
      $gte: new Date(date1 + ' 00:00:00'),
      $lte: new Date(date2 + ' 23:59:59')
    }})
    return res.json({status: true, data: leads})
  } catch (err) {
    return res.json({
      status: false,
      error: err.name,
      msg: err.message
    })
  }
})

router.put('/:id', async (req, res) => {
  try {
    const {kam, observacion, estado} = req.body
    const lead = await LeadDercoParts.findById(req.params.id)
    if (kam) lead.kam = kam
    if (observacion) lead.observacion = observacion
    if (estado) lead.estado = estado
    await lead.save()
    return res.json({ status: true, msg: 'Modificado Correctamente' })
  } catch (err) {
    return res.json({ status: false, error: err.message })
  }
})

router.post('/', async (req, res, next) => {
  LeadDercoParts.create(req.body, (err, post) => {
    if (err) return res.json({status: false, msg: 'No se guardo el registro', error: err.message})
    return res.json({status: true, data: post})
  })
})

router.delete('/:id', async (req, res) => {
  LeadDercoParts.findByIdAndRemove(req.params.id, (err, post) => {
    if (err) return res.json({status: false, msg: 'No se pudo eliminar', error: err.message})
    return res.json({status: true, msg: 'Eliminado correctamente'})
  })
})

module.exports = router
