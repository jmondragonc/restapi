/*eslint-disable */
var express = require("express");
var router = express.Router();
var Mazdamodel = require("../../models/MazdaCampaña/Mazda");
var NewMazdamodel = require("../../models/MazdaCampaña/NewMazdaModel");
const { getVersionByModel } = require("./requests");

router.get("/", async (req, res) => {
  const models = await Mazdamodel.find(
    {},
    { versiones: 0, _id: 0, createdAt: 0, updatedAt: 0 }
  );
  return res.json(models);
});

router.get("/getNewModels", async (req, res) => {
  const models = await NewMazdamodel.find(
    {},
    { versiones: 0, _id: 0, createdAt: 0, updatedAt: 0 }
  );
  return res.json(models);
});

router.get("/updateModels", async (req, res) => {
  try {
    await Mazdamodel.deleteMany();
    const models = require("../../assets/MazdaCampaing.json");
    Mazdamodel.create(models, (err, post) => {
      if (err) return next(err);
      return res.json(post);
    });
  } catch (error) {
    return { status: false, error: error.message };
  }
});

//*************    En desuso      ************ */
router.get("/getVersions/:slug", async (req, res) => {
  try {
    const model = await getVersionByModel(req.params.slug);
    let currentModel = await Mazdamodel.findOne({ modelo: req.params.slug });

    if (model.length > 0 && currentModel) {
      let modelFilter = model[0];
      const versionFilter = version =>
        currentModel.versiones.some(v => v.version == version.slug);
      modelFilter.versions = modelFilter.versions.filter(versionFilter);
      modelFilter.versions.forEach(v => {
        const versionFound = currentModel.versiones.find(
          vF => vF.version == v.slug
        );
        if (versionFound) {
          const { version, interior, exterior } = versionFound;
          console.log(version);
          v.campaingImgs = { exterior };
          if (interior.length !== 0)
            v.campaingImgs = { ...v.campaingImgs, interior };
        }
      });
      return res.json(modelFilter);
    } else {
      return res.json({ status: false, msg: "No existe modelo con este slug" });
    }
  } catch (err) {
    return res.json({
      status: false,
      error: { name: err.name, msg: err.message }
    });
  }
});

router.get("/getNewVersions/:slug", async (req, res) => {
  try {
    const model = await getVersionByModel(req.params.slug);
    let currentModel = await NewMazdamodel.findOne({ modelo: req.params.slug });

    if (model.length > 0 && currentModel) {
      let modelFilter = model[0];
      const versionFilter = version =>
        currentModel.versiones.some(v => v.version == version.slug);
      modelFilter.versions = modelFilter.versions.filter(versionFilter);
      modelFilter.versions.forEach(v => {
        const versionFound = currentModel.versiones.find(
          vF => vF.version == v.slug
        );
        if (versionFound) {
          const { gallery } = versionFound;
          v.gallery = {
            externas: gallery.external
          };
          if (gallery.internal.length !== 0)
            v.gallery = {
              ...v.gallery,
              interior: [
                {
                  medias: gallery.internal
                }
              ]
            };
          v.colors = gallery.colors;
        }
      });
      return res.json(modelFilter);
    } else {
      return res.json({ status: false, msg: "No existe modelo con este slug" });
    }
  } catch (err) {
    console.log(err);
    return res.json({
      status: false,
      error: { name: err.name, msg: err.message }
    });
  }
});

// update Version by IdVersion
router.put("/:id", async (req, res) => {
  const version = await NewMazdamodel.findOne({
    "versiones._id": req.params.id
  });
});

router.delete("/:id", async (req, res) => {
  NewMazdamodel.findByIdAndRemove(req.params.id, req.body, (err, post) => {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
