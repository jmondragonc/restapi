var express = require("express");
var router = express.Router();
var Mazdamodel = require("../../models/MazdaCampaña/NewMazdaModel");
const {
  getAllModels,
  getVersionByModel,
  getVersionInCatalogo
} = require("./requests");

router.get("/", async (req, res) => {
  const models = await Mazdamodel.find();
  return res.json(models);
});

router.post("/", async (req, res, next) => {
  try {
    await Mazdamodel.deleteMany();
    Mazdamodel.create(req.body, (err, post) => {
      if (err) return next(err);
      return res.json(post);
    });
  } catch (error) {
    console.log(error);
    return res.json({ status: false, error: error.message });
  }
});

router.post("/addOne", async(req, res, next) => {
  try {
    Mazdamodel.create(req.body, (err, post) => {
      if (err) return next(err);
      return res.json(post);
    })
  } catch (err) {
    console.log(error);
    return res.json({ status: false, error: error.message });
  }
})

router.get("/getAllmodels", async (req, res) => {
  try {
    let models = await getAllModels();
    models = models.filter(model => model.brandId === 8);
    models = models.map(model => model.slug);
    return res.json(models);
  } catch (error) {
    console.log(error);
    return res.json({ status: false, error: error.message });
  }
});

router.get("/updateModelBySlug/:slug", async (req, res) => {
  try {
    let model = await getVersionByModel(req.params.slug);
    model = model[0];
    if (model) {
      const modelFound = await Mazdamodel.findOne({ modelo: req.params.slug });
      if (!modelFound)
        return res.json({
          msg: "El modelo no se encuentra agregado en el cotizador"
        });
      const versions = model.versions.map(version => version.id);
      let versionsFound = [];
      const filterVersionsWithGallery = async version => {
        const resp = await getVersionInCatalogo(version);
        if (Array.isArray(resp)) {
          if (resp.length !== 0) {
            let ver = resp[0];
            const { external } = ver.gallery;
            const valitateField = field => field && field.length !== 0;
            if (valitateField(external)) {
              versionsFound = [
                ...versionsFound,
                { version: ver.slug, id: ver.id, gallery: ver.gallery }
              ];
            }
          }
        }
      };
      await Promise.all(versions.map(filterVersionsWithGallery));
      modelFound.versiones = versionsFound;
      await modelFound.save();
      return res.json({
        status: "Versiones actualizadas",
        versions: versionsFound
      });
    } else {
      return res.json({ msg: "No hay modelo con este slug" });
    }
  } catch (error) {
    console.log(error);
    return res.json({ status: false, error: error.message });
  }
});

module.exports = router;
