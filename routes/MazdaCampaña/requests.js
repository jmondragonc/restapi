const fetch = require("node-fetch");
const url = "https://middleware.derco.com.pe/";
const urld = 'http://middleware.derco.com.pe/api/v4/versions/catalogo/1188'

module.exports = {
  getAllModels : () => {
    return fetch(`${url}api/v6/models`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
    }).then(response => response.json());
  },
  getVersionByModel : (model) => {
    return fetch(`${url}api/v6/models/slug/${model}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
    }).then(response => response.json());
  },
  getVersionInCatalogo: (id) => {
    return fetch(`${url}api/v4/versions/catalogo/${id}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
    }).then(response => response.json());
  }
}

