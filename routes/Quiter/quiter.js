const express = require("express");
const router = express.Router();
const fetch = require("node-fetch");
const axios = require("axios");
const passport = require("passport");
const qs = require("qs");
// Models
const FamilyModel = require("../../models/Quiter/Family");
const SellerModel = require("../../models/Quiter/Seller");
const SubOriginModel = require("../../models/Quiter/Suborigin");

require("../../config/passport")(passport);

// Json to import
const familyJson = require("../../utils/familiQuiter.json");
const sellerJson = require("../../utils/sellerQuiter.json");
const subOriginJson = require("../../utils/subOriginQuiter.json");

var config = data => {
  return {
    method: "post",
    url: "https://apilead.gac-sa.cl/api/lead-crear",
    headers: {
      apikey: "whp55npf687em2d30474e0wc7j274w3mchh87638dt3dgnp142",
      usuario: "wigo",
      contrasena: "Yxe9wZ4ccL1D53df",
      idform: "5",
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: qs.stringify(data)
  };
};

router.get("/import", async (req, res, next) => {
  try {
    // Delete if exist
    await FamilyModel.deleteMany();
    FamilyModel.create(familyJson, (err, post) => {
      if (err) next(err);
      return res.json({
        status: "Ingresado Correctamente",
        data: post
      });
    });
  } catch (err) {
    console.log(err);
  }
});

router.get("/getAll/:collection", async (req, res, next) => {
  const {
    collection
  } = req.params;
  let data;
  switch (collection) {
    case "family":
      data = await FamilyModel.find();
      return res.json({
        status: "Ok",
        data
      });
    case "seller":
      data = await SellerModel.find();
      return res.json({
        status: "Ok",
        data
      });
    case "suborigin":
      data = await SubOriginModel.find();
      return res.json({
        status: "Ok",
        data
      });
    default:
      return res.json({
        status: "Error",
        msg: "This collection doesn't exist"
      });
  }
});

router.post("/", async (req, res, next) => {
  try {
    const {
      codigo_sap,
      brand_slug
    } = req.body;

    const toSlugFormat = (brand) => {
      let brand_slug = brand.toLowerCase()
      if (brand.includes(' ')) {
        brand_slug = brand_slug.replace(' ', '-')
      }
      return brand_slug
    }
    let brandToSlug = toSlugFormat(brand_slug)

    const family = await FamilyModel.findOne({
      codigo_sap: codigo_sap
    });
    const seller = await SellerModel.findOne({
      slug: brandToSlug
    });
    const subOrigin = await SubOriginModel.findOne({
      slug: brandToSlug
    });

    if (!family || !seller || !subOrigin)
      return res.json({
        status: false,
        msg: "No hay coincidencia con filtros",
      });
    const response = {
      family_code: family.code,
      sub_origen_id: subOrigin.id,
      vendedor_bolsa_id: seller.id
    };
    return res.json({
      status: true,
      data: response
    });
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.post("/quiterPost", async (req, res, next) => {
  await axios(config(req.body))
    .then(result => {
      return res.json({
        status: true,
        data: result.data
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        status: false,
        msg: err.message,
        name: err.name
      });
    });
});

// rutas crud para familia (admin de modelos de Gagsa)

router.get(
  "/retrieve",
  passport.authenticate("jwt", {
    session: false
  }),
  async (req, res, next) => {
    var token = getToken(req.headers);
    if (token) {
      const families = await FamilyModel.find();
      return res.status(200).json({
        success: true,
        data: families
      });
    } else {
      return res.status(401).json({
        success: false,
        msg: "unautenticated."
      });
    }
  }
);

router.get(
  "/retrieve/:page",
  passport.authenticate("jwt", {
    session: false
  }),

  async (req, res, next) => {
    var perPage = 20;
    var page = req.params.page || 1;

    var token = getToken(req.headers);
    if (token) {

      FamilyModel.find()
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .exec((err, families) => {
          if (err) return next(err);
          FamilyModel.countDocuments((err, count) => {
            if (err) return next(err);
            let totalPages = Math.ceil(count / perPage);
            return res.status(200).json({
              success: true,
              totalPages: totalPages,
              data: families
            });
          });
        });
    } else {
      return res.status(401).json({
        success: false,
        msg: "unautenticated."
      });
    }
  }
);

router.post(
  "/create",
  passport.authenticate("jwt", {
    session: false
  }),
  async (req, res, next) => {
    try {
      var token = getToken(req.headers);
      if (token) {
        const {
          brand,
          model,
          code,
          version,
          codigo_sap
        } = req.body;

        const family = new FamilyModel({
          brand,
          model,
          code,
          version,
          codigo_sap
        });

        family.save((err, post) => {
          if (err)
            return res.json({
              success: false,
              error: err
            });
          return res.json({
            success: true,
            data: post
          });
        });
      } else {
        return res.status(401).json({
          success: false,
          msg: "unautenticated."
        });
      }
    } catch (err) {
      return res.status(500).json({
        success: false,
        error: err
      });
    }
  }
);

router.put(
  "/edit/:id",
  passport.authenticate("jwt", {
    session: false
  }),
  async (req, res, next) => {
    try {
      var token = getToken(req.headers);
      if (token) {
        const {
          brand,
          model,
          code,
          version,
          codigo_sap
        } = req.body;

        const family = await FamilyModel.findById(req.params.id);
        if (family) {
          if (brand) family.brand = brand;
          if (model) family.model = model;
          if (code) family.code = code;
          if (version) family.version = version;
          if (codigo_sap) family.codigo_sap = codigo_sap;
        } else {
          return res.json({
            success: false,
            error: "Family with this id doesn't found"
          });
        }

        family.save((err, post) => {
          if (err)
            return res.json({
              success: false,
              error: err
            });
          return res.json({
            success: true,
            data: post
          });
        });
      } else {
        return res.status(401).json({
          success: false,
          msg: "unautenticated."
        });
      }
    } catch (err) {
      return res.status(500).json({
        success: false,
        error: err
      });
    }
  }
);

router.delete(
  "/remove/:id",
  passport.authenticate("jwt", {
    session: false
  }),
  async (req, res, next) => {
    try {
      var token = getToken(req.headers);
      if (token) {
        FamilyModel.findByIdAndRemove(req.params.id, async (err, post) => {
          if (err)
            return res.json({
              success: false,
              error: err
            });
          return res.json({
            success: true,
            data: post
          });
        });
      } else {
        return res.status(401).json({
          success: false,
          msg: "unautenticated."
        });
      }
    } catch (err) {
      return res.status(500).json({
        success: false,
        error: err
      });
    }
  }
);

module.exports = router;
