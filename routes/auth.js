/*eslint-disable */
var mongoose = require('mongoose')
var passport = require('passport')
var settings = require('../config/settings')
require('../config/passport')(passport)
var express = require('express')
var jwt = require('jsonwebtoken')
var router = express.Router()
var Admin = require("../models/Admin")
var AdminSuzuki = require("../models/AgendamientoSuzuki/AdminSuzuki")

router.post('/register', function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({success: false, msg: 'Please introduce username and password.'})
  } else {
    var newAdmin = new Admin({
      first_name: req.body.first_name,
      last_name: req.body.last_name, 
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
      brand: req.body.brand,
    })
    // save the user
    newAdmin.save(function(err) {
      if (err) {
        console.log(err, res)
        return res.json({success: false, msg: 'email already exists.'})
      }
      res.json({success: true, msg: 'Successful created new user.'})
    })
  }
})

router.post('/login', function(req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  //res.setHeader('Access-Control-Allow-Credentials', true);
  Admin.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err
    if (!user) {
      res.status(401).send({ success: false, msg: 'Authentication failed. User not found.' })
    } else {
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          var token = jwt.sign(user.toJSON(), settings.secret)
          res.json({ success: true, token: 'JWT ' + token, role: user.role, brand: user.brand,id: user._id })
        } else {
          res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' })
        }
      })
    }
  })
})  


router.post('/loginGagsa', function(req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  //res.setHeader('Access-Control-Allow-Credentials', true);
  Admin.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err
    if (!user) {
      return res.status(401).send({ success: false, msg: 'Authentication failed. User not found.' })
    } 
    if (user.role !== 'Gagsa') {
      return res.status(403).send({ success: false, msg: 'Authentication failed. User role not authorized' })
    }
    user.comparePassword(req.body.password, function (err, isMatch) {
      if (isMatch && !err) {
        var token = jwt.sign(user.toJSON(), settings.secret)
        res.json({ success: true, token: 'JWT ' + token, role: user.role, brand: user.brand,id: user._id })
      } else {
        res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' })
      }
    })
    
  })
})


router.post('/suzukiregister', async function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({success: false, msg: 'Please introduce username and password.'})
  } else {
    var newAdmin = new AdminSuzuki({
      first_name: req.body.first_name,
      last_name: req.body.last_name, 
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
      turn: req.body.role == 'Vendedor' ? true : false,
      marca: req.body.marca ? req.body.marca : null
    })
    try{
      if (req.body.role == 'Vendedor'){
        let adminFound = await AdminSuzuki.findOne({turn: true})
        if (adminFound) adminFound.turn = false
      } 
      await adminFound.save()
    }catch (err){
      console.log(err)
    }
    // save the user
    newAdmin.save(async function(err) {
      if (err) {
        console.log(err, res)
        return res.json({success: false, msg: 'email already exists.'})
      }
      res.json({success: true, msg: 'Successful created new user.'})
    })
  }
})

router.post('/suzukilogin', function(req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  //res.setHeader('Access-Control-Allow-Credentials', true);
  AdminSuzuki.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err
    if (!user) {
      res.status(401).send({ success: false, msg: 'Authentication failed. User not found.' })
    } else {
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          var token = jwt.sign(user.toJSON(), settings.secret)
          res.json({ success: true, token: 'JWT ' + token, role: user.role, id: user._id, nombre: user.first_name, apellido: user.last_name })
        } else {
          res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' })
        }
      })
    }
  })
})

module.exports = router
