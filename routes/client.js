/*eslint-disable */
var express = require('express')
var router = express.Router()
var mongoose = require('mongoose')
var Client = require('../models/Client.js')
var Seekop = require('../models/SeekopResponse.js');
var passport = require('passport')
var tiendas = require('../utils/Tiendas.json');
const cryptoRandomString = require('crypto-random-string');
var fetch = require("node-fetch");

const axios = require('axios')
const qs = require('qs')

// Models para quitter
const FamilyModel = require("../models/Quiter/Family");
const SellerModel = require("../models/Quiter/Seller");
const SubOriginModel = require("../models/Quiter/Suborigin");
const {
  API_URL
} = require('../config/contants.js');


require('../config/passport')(passport)

/* GET ALL CLIENTS */
router.get('/', passport.authenticate('jwt', {
  session: false
}), function (req, res) {
  var token = getToken(req.headers)
  if (token) {
    Client.find(function (err, clients) {
      if (err) return next(err)
      res.json(clients)
    })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'test.'
    })
  }
})

/* GET EXCEL CLIENTS */
router.get('/excel/:brand/:date1/:date2', passport.authenticate('jwt', {
  session: false
}), function (req, res) {
  var token = getToken(req.headers)
  if (token) {
    Client.find({
      brand_w2l: req.params.brand,
      created_at: {
        $gte: new Date(req.params.date1 + ' 00:00:00'),
        $lt: new Date(req.params.date2 + ' 23:59:59')
      }
    }, function (err, clients) {
      if (err) return next(err)
      res.json(clients)
    })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'excel generated.'
    })
  }
})

/* GET SEARCH CLIENTS */
router.get('/search/:brand/:date1/:date2/:page', passport.authenticate('jwt', {
  session: false
}), function (req, res) {
  var token = getToken(req.headers)
  if (token) {

    var perPage = 50
    var page = req.params.page || 1

    var brand = req.params.brand
    var brandFilter = brand === "CITROËN" || brand === "CITROEN" ? {
      brand_w2l: {
        $in: ["CITROËN", "CITROEN"]
      }
    } : {
      brand_w2l: brand
    }

    Client
      .find({
        ...brandFilter,
        created_at: {
          $gte: new Date(req.params.date1 + ' 00:00:00'),
          $lt: new Date(req.params.date2 + ' 23:59:59')
        }
      })
      .sort({
        created_at: -1
      })
      .skip((perPage * page) - perPage)
      .limit(perPage)
      .exec(function (err, clients) {
        if (err) return next(err)
        Client.countDocuments({
          ...brandFilter,
          created_at: {
            $gte: req.params.date1 + ' 00:00:00.000Z',
            $lt: new Date(req.params.date2 + ' 23:59:59')
          }
        }).exec(function (err, count) {
          if (err) return next(err)
          var total = Math.ceil(count / perPage);
          var len = clients.length
          var arrClients = []
          for (var i = 0; i < len; i++) {
            var d = new Date(clients[i].created_at);
            var n = d.toLocaleString('es-PE');
            arrClients.push({
              id: clients[i].id,
              id_w2l: clients[i].id_w2l,
              rut_w2l: clients[i].rut_w2l,
              first_name: clients[i].first_name,
              last_name: clients[i].last_name,
              fone1_w2l: clients[i].fone1_w2l,
              email: clients[i].email,
              state: clients[i].state,
              url1_w2l: clients[i].url1_w2l,
              url2_w2l: clients[i].url2_w2l,
              brand_w2l: clients[i].brand_w2l,
              model_w2l: clients[i].model_w2l,
              version_w2l: clients[i].version_w2l,
              sap_w2l: clients[i].sap_w2l,
              price_w2l: clients[i].price_w2l,
              local_w2l: clients[i].local_w2l,
              cc_destino: clients[i].cc_destino,
              cod_origen2_w2l: clients[i].cod_origen2_w2l,
              store: clients[i].store,
              terms: clients[i].terms,
              tipo_documento: clients[i].tipo_documento,
              razon_social: clients[i].razon_social,
              direccion: clients[i].direccion,
              distrito: clients[i].distrito,
              created_at: n,
              total: total,
              count: count
            })
          }
          res.json(arrClients)
        })
      })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'test.'
    })
  }
})


/* GET SEARCH ALL CLIENTS BY DATES */
router.get('/search/:brand/:date1/:date2', function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  var brand = req.params.brand
  var brandFilter = brand === "CITROËN" || brand === "CITROEN" ? {
    brand_w2l: {
      $in: ["CITROËN", "CITROEN"]
    }
  } : {
    brand_w2l: brand
  }

  Client
    .find({
      ...brandFilter,
      created_at: {
        $gte: new Date(req.params.date1 + ' 00:00:00'),
        $lt: new Date(req.params.date2 + ' 23:59:59')
      }
    }, function (err, post) {
      if (err) return next(err)
      res.json(post)
    })
})

/* GET SINGLE CLIENT BY ID */
router.get('/user/:id', passport.authenticate('jwt', {
  session: false
}), function (req, res, next) {
  var token = getToken(req.headers)
  if (token) {
    Client.findById(req.params.id, function (err, post) {
      if (err) return next(err)
      var d = new Date(post.created_at);
      var n = d.toLocaleString('es-PE');
      var npost = {
        id: post.id,
        id_w2l: post.id_w2l,
        rut_w2l: post.rut_w2l,
        first_name: post.first_name,
        last_name: post.last_name,
        fone1_w2l: post.fone1_w2l,
        email: post.email,
        state: post.state,
        url1_w2l: post.url1_w2l,
        url2_w2l: post.url2_w2l,
        brand_w2l: post.brand_w2l,
        model_w2l: post.model_w2l,
        version_w2l: post.version_w2l,
        sap_w2l: post.sap_w2l,
        price_w2l: post.price_w2l,
        local_w2l: post.local_w2l,
        cc_destino: post.cc_destino,
        cod_origen2_w2l: post.cod_origen2_w2l,
        store: post.store,
        terms: post.terms,
        tipo_documento: post.tipo_documento,
        razon_social: post.razon_social,
        direccion: post.direccion,
        distrito: post.distrito,
        created_at: n
      }
      res.json(npost)
    })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'test.'
    })
  }
})

//SAVE CLIENT FROM INTERNET
router.post('/', async function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  //res.setHeader('Access-Control-Allow-Credentials', true);

  // Validacion de duplicidad

  console.log("BODY", req.body);

  try {
    const isDuplicated = await haveDuplicate(req.body)
    if (isDuplicated) return res.json(req.body)
  } catch (err) {
    console.log(err)
  }

  Client.create({
    ...req.body,
    seekopId: uniqueId()
  }, async (err, post) => {
    if (err) return next(err)

    //***********     Envio a Seekop    *************** */
    let postData = {};

    try {
      const tienda = tiendas.find(t => t.codigo === req.body.store);
      if (tienda) {

        const seekopres = await sendData(post._doc, tienda);
        if (seekopres.status === 'ok') {
          postData = {
            seekopEnvio: 'Enviado'
          }
        } else {
          postData = {
            seekopEnvio: 'No enviado'
          }
        }

        const seekopNew = new Seekop({
          status: seekopres.status ? seekopres.status : null,
          message: seekopres.message ? seekopres.message : null,
          id: seekopres.id ? seekopres.id : null,
          store: post.store,
          url1_w2l: post.url1_w2l,
        })

        await seekopNew.save();
      } else {
        postData = post
      }
    } catch (err) {
      console.log(err.message)
    }
    //************************************************ */

    //***********     Envio a Quitter    *************** */

    try {

      if (req.body.store === 'PE44') {

        const toSlugFormat = (brand) => {
          let brand_slug = brand.toLowerCase()
          if (brand.includes(' ')) {
            brand_slug = brand_slug.replace(' ', '-')
          }
          return brand_slug
        }

        let brandToSlug = toSlugFormat(req.body.marca2)

        if (brandToSlug === 'citroën') {
          brandToSlug = 'citroen'
        }

        const family = await FamilyModel.findOne({
          codigo_sap: req.body.sap_w2l
        });
        const seller = await SellerModel.findOne({
          slug: brandToSlug
        });
        const subOrigin = await SubOriginModel.findOne({
          slug: brandToSlug
        });

        if (!family || !seller || !subOrigin) {
          postData = {
            ...postData._doc,
            QuiterEnvio: {
              msg: 'No enviado, no hay concidencias en familia, vendedor o suborigen',
            }
          }
        } else {
          var config = (data) => {
            return {
              method: "post",
              url: "https://apilead.gac-sa.cl/api/lead-crear",
              headers: {
                apikey: "whp55npf687em2d30474e0wc7j274w3mchh87638dt3dgnp142",
                usuario: "wigo",
                contrasena: "Yxe9wZ4ccL1D53df",
                idform: "5",
                "Content-Type": "application/x-www-form-urlencoded",
              },
              data: qs.stringify(data),
            };
          };


          const saveDataQuiter = {
            dato_centro: 809,
            dato_cliente_email: req.body.email,
            dato_cliente_nombre: `${req.body.first_name} ${req.body.last_name}`,
            dato_cliente_rut: req.body.rut_w2l,
            dato_cliente_telefono: req.body.fone1_w2l,
            dato_id_suborigen: subOrigin.id,
            dato_id_vendedor_bolsa: seller.id,
            dato_observaciones: null,
            dato_tipo_documento: 'NUEVO',
            dato_vehiculo_familia: family.code,
            dato_vehiculo_marca: req.body.marca2,
            dato_vehiculo_modelo: req.body.model_w2l
          }

          await axios(config(saveDataQuiter))
            .then((result) => {
              postData = {
                ...postData._doc,
                QuiterEnvio: {
                  data: result.data
                }
              }
            })
            .catch((err) => {
              postData = {
                ...postData._doc,
                QuiterEnvio: {
                  msg: err.message,
                  name: err.name
                }
              }
            });
        }
      }
    } catch (err) {
      console.log(err.message)
    }

    //************************************************ */

    //***********     Envio a Upnify    *************** */

    try {

      if (req.body.store === 'PE09' || req.body.store === 'PE10' || req.body.store === 'PE08' || req.body.store === 'PE11') {

        var configUpnify = (data) => {
          return {
            method: "post",
            url: API_URL.UPNIFY,
            headers: {
              "Content-Type": "application/json",
            },
            data: JSON.stringify(data),
          };
        };

        const saveDataUpnify = {
          nombres: req.body.first_name,
          apellidos: req.body.last_name,
          telefono: req.body.fone1_w2l,
          correo: req.body.email,
          direccion: req.body.direccion,
          tipo_documento: req.body.tipo_documento,
          numero_documento: req.body.rut_w2l,
          marca: req.body.marca2,
          modelo: req.body.model_w2l,
          version: req.body.version_w2l,
          codigo_sap: req.body.sap_w2l,
          distrito: req.body.distrito,
          tienda: req.body.store,
          local: req.body.local_w2l,
          clausulas: req.body.terms
        }

        await axios(configUpnify(saveDataUpnify))
          .then(async (result) => {
            try {
              if (Array.isArray(result.data)) {
                if (Array.isArray(result.data[0].details))
                  post.upnifyId = result.data[0].details[0].tkContacto
                await post.save()
              }
            } catch (err) {
              console.log("🚀 ~ file: client.js ~ line 347 ~ .then ~ err", err)
            }
          })
          .catch((err) => {
            console.log("🚀 ~ file: client.js ~ line 335 ~ Client.create ~ err", err)
          });
      }
    } catch (err) {
      console.log(err.message)
    }


    //************************************************ */

    return res.json(postData)
    // fetch('http://google.com/')
    // .then(function(response) {
    //   console.log("TEST", response)
    // }, function(error) {
    //   error.message //=> String
    // })

  })

})

/* UPDATE CLIENT */
router.put('/:id', passport.authenticate('jwt', {
  session: false
}), function (req, res, next) {
  var token = getToken(req.headers)
  if (token) {
    Client.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
      if (err) return next(err)
      res.json(post)
    })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'test.'
    })
  }
})

/* DELETE CLIENT */
router.delete('/:id', passport.authenticate('jwt', {
  session: false
}), function (req, res, next) {
  var token = getToken(req.headers)
  if (token) {
    Client.findByIdAndRemove(req.params.id, req.body, function (err, post) {
      if (err) return next(err)
      res.json(post)
    })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'test.'
    })
  }
})

router.put('/updateFinancing/:id', async (req, res) => {
  try {
    const client = await Client.findOne({
      id_w2l: req.params.id
    })
    if (client) {
      const data = {
        amount_to_finance: req.body.amount_to_finance,
        birthday_date: req.body.birthday_date,
        business_name: req.body.business_name,
        init_work_year: req.body.init_work_month,
        salary: req.body.salary,
        car_value: req.body.car_value,
        initial_fee: req.body.initial_fee,
        labor_regime: req.body.labor_regime,
        init_work_month: req.body.init_work_month
      }
      client.financing = data
      await client.save()
      return res.status(200).json({
        success: true,
        data: client
      })
    } else {
      return res.status(400).json({
        success: false,
        msg: 'No existe lead con este id web'
      })
    }
  } catch (error) {
    return res.status(500).json({
      success: false,
      error: error.message
    })
  }
})

router.put('/updateRetakes/:id', async function (req, res, next) {
  try {
    const client = await Client.findOne({
      id_w2l: req.params.id
    })
    if (client) {
      const data = {
        placa: req.body.license_plate,
        marca: req.body.brand,
        modelo: req.body.model,
        año: req.body.year,
        km2: req.body.mileage,
        combustible: req.body.fuel,
        transmisión: req.body.transmission,
        terminos: req.body.terms,
        estado: req.body.Estado,
      }
      client.retakes = data
      await client.save()
      return res.status(200).json({
        success: true,
        data: client
      })
    } else {
      return res.status(400).json({
        success: false,
        msg: 'No existe lead con este id web'
      })
    }
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: err.message
    })
  }
})



sendData = (post, tienda) => {

  const {
    _id,
    first_name,
    last_name,
    fone1_w2l,
    email,
    marca2,
    model_w2l,
    version_w2l,
    tipo_documento,
    rut_w2l,
    seekopId
  } = post


  let dataPost = {
    prospect: {
      status: "new",
      id: seekopId, // Falta el id
      requestdate: formatDate() /* "2020-09-03" */,
      vehicle: {
        interest: "buy",
        status: "new",
        make: marca2,
        trim: version_w2l,
        model: model_w2l
      },
      customer: {
        contact: {
          name: [{
            part: "first",
            value: first_name
          },
          {
            part: "last",
            value: last_name
          }
          ],
          email: email,
          dni: rut_w2l,
          phone: [
            fone1_w2l
          ]
        },
        comments: "Tipo de Documento: " + tipo_documento,
        origin: "",
        privacy: "si",
        terms: "si"
      },
      vendor: {
        source: "derco",
        id: tienda.codigo,
        name: tienda.nombre
      }
    },
    provider: {
      name: "derco"
    }
  }

  return fetch('https://www.sicopweb.com/apidms/dms/v1/rest/leads/adfv2', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
      //Authorization: token ? `Bearer ${token}` : '',
    },
    body: JSON.stringify(dataPost),
  }).then(response => response.json())
}

formatDate = () => {
  var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [year, month, day].join('-');
}

uniqueId = () => {
  let id = cryptoRandomString({
    length: 10,
    type: 'alphanumeric'
  }) + (+new Date).toString(36).slice(-5);
  return id;
};


getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

const haveDuplicate = async (body) => {
  //const {first_name, last_name, tipo_documento, rut_w2l, fone1_w2l, email, url1_w2l, model_w2l, version_w2l, sap_w2l, store, local_w2l} = body
  const user = await Client.findOne(body)
  return (user !== null)
}

module.exports = router
