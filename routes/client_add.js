/*eslint-disable */
var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Client = require("../models/Client.js");
var passport = require("passport");
var tiendas = require("../utils/Tiendas.json");
const cryptoRandomString = require("crypto-random-string");
var fetch = require("node-fetch");

const axios = require('axios')
const qs = require('qs')

// Models para quitter
const FamilyModel = require("../models/Quiter/Family");
const SellerModel = require("../models/Quiter/Seller");
const SubOriginModel = require("../models/Quiter/Suborigin");

require("../config/passport")(passport);

//SAVE CLIENT FROM INTERNET
router.post("/", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers);
  if (token) {
    Client.create(req.body, async (err, post) => {
      if (err) return next(err);

      //***********     Envio a Seekop    *************** */
      try {
        const tienda = tiendas.find(t => t.codigo === req.body.store);
        if (tienda) {
          const seekopres = await sendData(req.body, tienda);
          if (seekopres.status === "ok") {
            post = Object.assign(post, { seekopEnvio: "Enviado" });
          } else {
            post = Object.assign(post, { seekopEnvio: "No enviado" });
          }
        } else {
          post = Object.assign(post, {
            seekopEnvio: "No enviado por no tienda"
          });
        }
      } catch (err) {
        console.log(err.message);
      }

      //************************************************ */

      //***********     Envio a Quitter    *************** */

      try {
        if (req.body.store === "PE44") {
          const toSlugFormat = brand => {
            let brand_slug = brand.toLowerCase();
            if (brand.includes(" ")) {
              brand_slug = brand_slug.replace(" ", "-");
            }
            return brand_slug;
          };

          let brandToSlug = toSlugFormat(req.body.marca2);

          if (brandToSlug === "citroën") {
            brandToSlug = "citroen";
          }

          const family = await FamilyModel.findOne({
            model: req.body.model_w2l
          });
          const seller = await SellerModel.findOne({ slug: brandToSlug });
          const subOrigin = await SubOriginModel.findOne({ slug: brandToSlug });

          if (!family || !seller || !subOrigin) {
            post = {
              ...post,
              QuiterEnvio: {
                msg:
                  "No enviado, no hay concidencias en familia, vendedor o suborigen"
              }
            };
          } else {
            var config = data => {
              return {
                method: "post",
                url: "https://apilead.gac-sa.cl/api/lead-crear",
                headers: {
                  apikey: "whp55npf687em2d30474e0wc7j274w3mchh87638dt3dgnp142",
                  usuario: "wigo",
                  contrasena: "Yxe9wZ4ccL1D53df",
                  idform: "5",
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                data: qs.stringify(data)
              };
            };

            const saveDataQuiter = {
              dato_centro: 809,
              dato_cliente_email: req.body.email,
              dato_cliente_nombre: `${req.body.first_name} ${
                req.body.last_name
              }`,
              dato_cliente_rut: req.body.razon_social,
              dato_cliente_telefono: req.body.fone1_w2l,
              dato_id_suborigen: subOrigin.id,
              dato_id_vendedor_bolsa: seller.id,
              dato_observaciones: null,
              dato_tipo_documento: "NUEVO",
              dato_vehiculo_familia: family.code,
              dato_vehiculo_marca: req.body.marca2,
              dato_vehiculo_modelo: req.body.model_w2l
            };

            await axios(config(saveDataQuiter))
              .then(result => {
                post = {
                  ...post,
                  QuiterEnvio: {
                    data: result.data
                  }
                };
              })
              .catch(err => {
                post = {
                  ...post,
                  QuiterEnvio: {
                    msg: err.message,
                    name: err.name
                  }
                };
              });
          }
        }
      } catch (err) {
        console.log(err.message);
      }

      //************************************************ */

      return res.json(post);
    });
  } else {
    return res.status(403).send({ success: false, msg: "Unauthorized." });
  }
});

getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

sendData = (post, tienda) => {
  const {
    _id,
    first_name,
    last_name,
    fone1_w2l,
    email,
    marca2,
    model_w2l,
    version_w2l,
    tipo_documento,
    rut_w2l
  } = post;

  let dataPost = {
    prospect: {
      status: "new",
      id: uniqueId(), // Falta el id
      requestdate: formatDate() /* "2020-09-03" */,
      vehicle: {
        interest: "buy",
        status: "new",
        make: marca2,
        trim: version_w2l,
        model: model_w2l
      },
      customer: {
        contact: {
          name: [
            {
              part: "first",
              value: first_name
            },
            {
              part: "last",
              value: last_name
            }
          ],
          email: email,
          dni: rut_w2l,
          phone: [fone1_w2l]
        },
        comments: "Tipo de Documento: " + tipo_documento,
        origin: "",
        privacy: "si",
        terms: "si"
      },
      vendor: {
        source: "derco",
        id: tienda.codigo,
        name: tienda.nombre
      }
    },
    provider: {
      name: "derco"
    }
  };

  return fetch("https://www.answerspip.com/apidms/dms/v1/rest/leads/adfv2", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
      //Authorization: token ? `Bearer ${token}` : '',
    },
    body: JSON.stringify(dataPost)
  }).then(response => response.json());
};

formatDate = () => {
  var d = new Date(),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
};

uniqueId = () => {
  let id =
    cryptoRandomString({ length: 10, type: "alphanumeric" }) +
    (+new Date()).toString(36).slice(-5);
  return id;
};

module.exports = router;
