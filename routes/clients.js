/*eslint-disable */
var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Client = require("../models/Client.js");
var tiendas = require("../utils/Tiendas.json");
var concesioraios = require("../utils/Concesionarios.json");
var passport = require("passport");
const cryptoRandomString = require("crypto-random-string");
var fetch = require("node-fetch");

require("../config/passport")(passport);

router.get("/", function (req, res) {
  Client.find(function (err, clients) {
    if (err) return next(err);
    res.json(clients);
  });
});

// rutas para metronic

router.post("/filter", async (req, res) => {
  const { concesionario, url1_w2l, store, date1, date2, marca2 } = req.body;

  try {
    let Query = {
      created_at: {
        $gte: new Date(date1 + " 00:00:00"),
        $lt: new Date(date2 + " 23:59:59")
      }
    };

    if (concesionario) {
      const concesionarioFound = concesioraios.find(
        t => t.id_society == concesionario
      );
      if (concesionarioFound) {
        Object.assign(Query, { store: { $in: concesionarioFound.stores } });
      }
    } else {
      if (store && store !== "") {
        Array.isArray(store)
          ? Object.assign(Query, { store: { $in: store } })
          : Object.assign(Query, { store: store });
      }
    }

    if (url1_w2l && url1_w2l !== "") {
      Array.isArray(url1_w2l)
        ? Object.assign(Query, { url1_w2l: { $in: url1_w2l } })
        : Object.assign(Query, { url1_w2l: url1_w2l });
    }

    if (marca2 && marca2 !== "Todas" && marca2 !== "") {
      Array.isArray(marca2)
        ? Object.assign(Query, { marca2: { $in: marca2 } })
        : Object.assign(Query, { marca2: marca2 });
    }

    const clients = await Client.find(Query).sort({ created_at: -1 });

    var arrClients = [];
    for (var i = 0; i < clients.length; i++) {
      /*   let total = await Client.countDocuments({ url1_w2l: url1_w2l,  store: store, created_at: { $gte: new Date(date1 + ' 00:00:00'), $lt: new Date(date2 + ' 23:59:59') } });
        total = Math.ceil(total / perPage); */
      var d = new Date(clients[i].created_at);
      var n = d.toLocaleString("es-PE");
      var ud = new Date(clients[i].updated_at);
      var un = ud.toLocaleString("es-PE");
      arrClients.push({
        id: i + 1,
        _id: clients[i]._id,
        id_w2l: clients[i].id_w2l,
        rut_w2l: clients[i].rut_w2l,
        first_name: clients[i].first_name,
        last_name: clients[i].last_name,
        fone1_w2l: clients[i].fone1_w2l,
        email: clients[i].email,
        state: clients[i].state,
        url1_w2l: clients[i].url1_w2l,
        url2_w2l: clients[i].url2_w2l,
        brand_w2l: clients[i].brand_w2l,
        marca2: clients[i].marca2,
        model_w2l: clients[i].model_w2l,
        version_w2l: clients[i].version_w2l,
        sap_w2l: clients[i].sap_w2l,
        price_w2l: clients[i].price_w2l,
        local_w2l: clients[i].local_w2l,
        cc_destino: clients[i].cc_destino,
        cod_origen2_w2l: clients[i].cod_origen2_w2l,
        store: clients[i].store,
        terms: clients[i].terms,
        tipo_documento: clients[i].tipo_documento,
        razon_social: clients[i].razon_social,
        direccion: clients[i].direccion,
        distrito: clients[i].distrito,
        estado: clients[i].estado,
        kam: clients[i].kam,
        observacion: clients[i].observacion,
        updateFecha: clients[i].updateFecha,
        created_at: n,
        updated_at: un
      });
    }

    const post = {
      meta: {
        page: 1,
        pages: 1,
        perpage: -1,
        total: clients.length,
        sort: "asc",
        field: "id"
      },
      data: arrClients
    };

    res.json(post);
  } catch (err) {
    res.json({ msg: "algo fue mal", err });
  }
});

router.post("/filterTest", async (req, res) => {
  const {
    concesionario,
    url1_w2l,
    store,
    date1,
    date2,
    marca2,
    salesforce,
    prioridad
  } = req.body;

  try {
    let Query = {
      created_at: {
        $gte: new Date(date1 + " 00:00:00"),
        $lt: new Date(date2 + " 23:59:59")
      }
    };

    if (store && store !== "") {
      Array.isArray(store)
        ? Object.assign(Query, { store: { $in: store } })
        : Object.assign(Query, { store: store });
    } else {
      if (concesionario) {
        const concesionarioFound = concesioraios.find(
          t => t.id_society == concesionario
        );
        if (concesionarioFound) {
          Object.assign(Query, { store: { $in: concesionarioFound.stores } });
        }
      }
    }

    if (url1_w2l && url1_w2l !== "") {
      Array.isArray(url1_w2l)
        ? Object.assign(Query, { url1_w2l: { $in: url1_w2l } })
        : Object.assign(Query, { url1_w2l: url1_w2l });
    }

    if (marca2 && marca2 !== "Todas" && marca2 !== "") {
      Array.isArray(marca2)
        ? Object.assign(Query, { marca2: { $in: marca2 } })
        : Object.assign(Query, { marca2: marca2 });
    }
    // console.log(salesforce, prioridad)
    if (salesforce && salesforce !== "") {
      Object.assign(Query, { salesforce: salesforce });
    }

    if (prioridad && prioridad !== "") {
      Object.assign(Query, { prioridad: prioridad });
    }

    const clients = await Client.find(Query, { __v: 0, vendedor: 0 }).sort({
      created_at: -1
    });

    const post = {
      meta: {
        page: 1,
        pages: 1,
        perpage: -1,
        total: clients.length,
        sort: "asc",
        field: "id"
      },
      data: clients
    };

    res.json(post);
  } catch (err) {
    res.json({ msg: "algo fue mal", err });
  }
});

router.post("/filter-all", async (req, res) => {
  const { url1_w2l, date1, date2 } = req.body;

  try {
    const clients = await Client.find({
      url1_w2l: url1_w2l,
      created_at: {
        $gte: new Date(date1 + " 00:00:00"),
        $lt: new Date(date2 + " 23:59:59")
      }
    }).sort({ created_at: -1 });

    var arrClients = [];
    for (var i = 0; i < clients.length; i++) {
      var d = new Date(clients[i].created_at);
      var n = d.toLocaleString("es-PE");
      var ud = new Date(clients[i].updated_at);
      var un = ud.toLocaleString("es-PE");
      arrClients.push({
        id: i + 1,
        _id: clients[i]._id,
        id_w2l: clients[i].id_w2l,
        rut_w2l: clients[i].rut_w2l,
        first_name: clients[i].first_name,
        last_name: clients[i].last_name,
        fone1_w2l: clients[i].fone1_w2l,
        email: clients[i].email,
        state: clients[i].state,
        url1_w2l: clients[i].url1_w2l,
        url2_w2l: clients[i].url2_w2l,
        brand_w2l: clients[i].brand_w2l,
        marca2: clients[i].marca2,
        model_w2l: clients[i].model_w2l,
        version_w2l: clients[i].version_w2l,
        sap_w2l: clients[i].sap_w2l,
        price_w2l: clients[i].price_w2l,
        local_w2l: clients[i].local_w2l,
        cc_destino: clients[i].cc_destino,
        cod_origen2_w2l: clients[i].cod_origen2_w2l,
        store: clients[i].store,
        terms: clients[i].terms,
        tipo_documento: clients[i].tipo_documento,
        razon_social: clients[i].razon_social,
        direccion: clients[i].direccion,
        distrito: clients[i].distrito,
        estado: clients[i].estado,
        kam: clients[i].kam,
        observacion: clients[i].observacion,
        updateFecha: clients[i].updateFecha,
        created_at: n,
        updated_at: un
      });
    }

    const post = {
      meta: {
        page: 1,
        pages: 1,
        perpage: -1,
        total: clients.length,
        sort: "asc",
        field: "id"
      },
      data: arrClients
    };

    res.json(post);
  } catch (err) {
    res.json({ msg: "algo fue mal", err });
  }
});

router.post("/leadsFilter", async (req, res) => {
  const { concesionario, url1_w2l, store, date1, date2, salesforce, prioridad } = req.body;

  try {
    let Query = {
      created_at: {
        $gte: new Date(date1 + " 00:00:00"),
        $lt: new Date(date2 + " 23:59:59")
      }
    };

    // yyyy-mm-dd

    if (concesionario) {
      const concesionarioFound = concesioraios.find(
        t => t.id_society == concesionario
      );

      if (concesionarioFound) {
        Object.assign(Query, { store: { $in: concesionarioFound.stores } });
      }
    } else {
      if (store && store !== "") {
        Array.isArray(store)
          ? Object.assign(Query, { store: { $in: store } })
          : Object.assign(Query, { store: store });
      }
    }

    if (url1_w2l) {
      Object.assign(Query, { url1_w2l: url1_w2l });
    }

    if (prioridad && prioridad !== "") {
      Object.assign(Query, { prioridad: prioridad });
    }

    if (salesforce && salesforce !== "") {
      Object.assign(Query, { salesforce: salesforce });
    }


    const clients = await Client.aggregate([
      { $match: Query },
      {
        $project: {
          _id: 1,
          id_w2l: 1,
          tipo_documento: 1,
          rut_w2l: 1,
          razon_social: 1,
          first_name: 1,
          last_name: 1,
          fone1_w2l: 1,
          email: 1,
          url1_w2l: 1,
          url2_w2l: 1,
          brand_w2l: 1,
          marca2: 1,
          model_w2l: 1,
          version_w2l: 1,
          sap_w2l: 1,
          store: 1,
          price_w2l: 1,
          state: 1,
          local_w2l: 1,
          distrito: 1,
          direccion: 1,
          cod_origen2_w2l: 1,
          terms: 1,
          created_at: 1,
          updated_at: 1,
          updateFecha: 1,
          retakes: 1,
          financing: 1,
          salesforce: 1,
          prioridad: 1
          /* created_at: {
            $dateToString: {
              format: "%Y-%m-%d",
              date: "$created_at"
            }
          },
          updated_at: {
            $dateToString: {
              format: "%Y-%m-%d",
              date: "$updated_at"
            }
          },
          updateFecha: {
            nuevo_date: {
              $dateToString: {
                format: "%Y-%m-%d",
                date: "$updateFecha.nuevo_date"
              }
            },
            contactado_date: {
              $dateToString: {
                format: "%Y-%m-%d",
                date: "$updateFecha.contactado_date"
              }
            },
            gestionado_date: {
              $dateToString: {
                format: "%Y-%m-%d",
                date: "$updateFecha.gestionado_date"
              }
            },
            cotizado_date: {
              $dateToString: {
                format: "%Y-%m-%d",
                date: "$updateFecha.cotizado_date"
              }
            },
            facturado_date: {
              $dateToString: {
                format: "%Y-%m-%d",
                date: "$updateFecha.facturado_date"
              }
            },
            cancelado_date: {
              $dateToString: {
                format: "%Y-%m-%d",
                date: "$updateFecha.cancelado_date"
              }
            } 
          }
          */
        }
      },
      { $sort: { created_at: -1 } }
    ]);

    //const clients = await Client.find(Query, {__v: 0, vendedor: 0,"updateFecha.nuevo_date": 0}).sort({ created_at: -1 });
    res.json(clients);
  } catch (err) {
    res.json({ msg: "Hubo un error", err });
  }
});

router.put("/updateEstado", async (req, res) => {
  const { id, estado, kam, observacion, prioridad, salesforce } = req.body;
  try {
    const client = await Client.findById(id);
    if (client) {
      if (kam) client.kam = kam;
      if (observacion) client.observacion = observacion;
      if (prioridad) client.prioridad = prioridad;
      if (salesforce) client.salesforce = salesforce;
      if (estado) {
        switch (estado) {
          case "Nuevo":
            client.estado = estado;
            break;
          case "Contactado":
            client.estado = estado;
            client.updateFecha.contactado_date = new Date();
            client.updated_at = new Date();
            break;
          case "Gestionado":
            client.estado = estado;
            if (!client.updateFecha.contactado_date)
              client.updateFecha.contactado_date = new Date();
            client.updateFecha.gestionado_date = new Date();
            client.updated_at = new Date();
            break;
          case "Cotizado":
            client.estado = estado;
            client.updateFecha.cotizado_date = new Date();
            if (!client.updateFecha.contactado_date)
              client.updateFecha.contactado_date = new Date();
            if (!client.updateFecha.gestionado_date)
              client.updateFecha.gestionado_date = new Date();
            client.updated_at = new Date();
            break;
          case "Facturado":
            client.estado = estado;
            client.updateFecha.facturado_date = new Date();
            client.updated_at = new Date();
            break;
          case "Cancelado":
            client.estado = estado;
            client.updateFecha.cancelado_date = new Date();
            client.updated_at = new Date();
            break;
          case "Generado":
            /* client.estado = estado;
            client.updateFecha.cancelado_date = new Date();
            client.updated_at = new Date(); */
            break;
          default:
            res.json({
              msg:
                "No se modifico ninguna fecha , verifica que el estado sea valido"
            });
        }
      }
      await client.save();
      res.json({ status: true, msg: "Modificado correctamente" });
    } else {
      res.json({ status: false, msg: "No existe usuario con este id" });
    }
  } catch (err) {
    console.log(err);
    res.json({ status: false, msg: "Hubo un error", err });
  }
});

router.put("/updateVendedor", async (req, res) => {
  const { id, vendedor } = req.body;
  try {
    const client = await Client.findById(id);
    if (client) {
      if (vendedor) client.vendedor = vendedor;
      await client.save();
      return res.json({ status: true, msg: "Modificado correctamente" });
    } else {
      return res.json({ status: false, msg: "No existe usuario con este id" });
    }
  } catch (err) {
    console.log(err);
    return res.json({ status: false, msg: "Hubo un error", err });
  }
});

router.post("/totalesGeneral", async (req, res) => {
  const { url1_w2l, date1, date2, store, marca2, unicos, prioridad, salesforce } = req.body;

  try {

    let Query = {
      created_at: {
        $gte: new Date(date1 + " 00:00:00"),
        $lt: new Date(date2 + " 23:59:59")
      }
    };

    if (marca2 && marca2 !== "Todas" && marca2 !== "") {
      Array.isArray(marca2)
        ? Object.assign(Query, { marca2: { $in: marca2 } })
        : Object.assign(Query, { marca2: marca2 });
    }

    if (store && store !== "") {
      Array.isArray(store)
        ? Object.assign(Query, { store: { $in: store } })
        : Object.assign(Query, { store: store });
    }

    if (salesforce && salesforce !== "") {
      Object.assign(Query, { salesforce: salesforce * 1 });
    }

    if (prioridad && prioridad !== "") {
      Object.assign(Query, { prioridad: prioridad * 1 });
    }

    let aggregateQuery = [{ $match: Query }];

    if (unicos && unicos === 1) {
      aggregateQuery = [
        ...aggregateQuery,
        {
          $group: {
            _id: {
              tipo_documento: "$tipo_documento",
              rut_w2l: "$rut_w2l",
              first_name: "$first_name",
              last_name: "$last_name",
              fone1_w2l: "$fone1_w2l",
              email: "$email",
              url1_w2l: "$url1_w2l",
              url2_w2l: "$url2_w2l",
              brand_w2l: "$brand_w2l",
              marca2: "$marca2",
              model_w2l: "$model_w2l",
              version_w2l: "$version_w2l",
              sap_w2l: "$sap_w2l",
              store: "$store",
              price_w2l: "$price_w2l",
              state: "$state",
              local_w2l: "$local_w2l",
              salesforce: "$salesforce",
              prioridad: "$prioridad",
              direccion: "$direccion"
            },
            count: { $sum: 1 }
          }
        },
        {
          $project: {
            _id: 0,
            tipo_documento: "$_id.tipo_documento",
            rut_w2l: "$_id.rut_w2l",
            first_name: "$_id.first_name",
            last_name: "$_id.last_name",
            fone1_w2l: "$_id.fone1_w2l",
            email: "$_id.email",
            url1_w2l: "$_id.url1_w2l",
            marca2: "$_id.marca2",
            model_w2l: "$_id.model_w2l",
            version_w2l: "$_id.version_w2l",
            sap_w2l: "$_id.sap_w2l",
            store: "$_id.store",
            price_w2l: "$_id.price_w2l",
            local_w2l: "$_id.local_w2l",
            direccion: "$_id.direccion",
            salesforce: "$_id.salesforce",
            prioridad: "$_id.prioridad",
            count: "$count"
          }
        }
      ];
    }

    const clients = await Client.aggregate(aggregateQuery);
    return res.json(clients);
  } catch (err) {
    return res.json({ msg: "algo fue mal", err: err.message });
  }
});

router.post("/getSuzukiLeads", async (req, res) => {
  const { date1, date2, brand } = req.body;
  try {
    let Query = {
      url1_w2l: { $regex: /showroom/, $options: "i" },
      marca2: brand ? brand : "SUZUKI"
    };

    if (date1 && date2) {
      Object.assign(Query, {
        updated_at: {
          $gte: new Date(date1 + " 00:00:00"),
          $lt: new Date(date2 + " 23:59:59")
        }
      });
    }

    const clients = await Client.find(Query, { __v: 0, updateFecha: 0 });
    return res.json(clients);
  } catch (err) {
    console.log(err);
    res.json({ msg: "algo fue mal", err: err.message });
  }
});

router.post(
  "/getCitroenLeads",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const { date1, date2 } = req.body;
    try {
      let Query = { url1_w2l: "https://citroen.com.pe/ofertas-2/" };
      if (date1 && date2) {
        Object.assign(Query, {
          created_at: {
            $gte: new Date(date1 + " 00:00:00"),
            $lt: new Date(date2 + " 23:59:59")
          }
        });
      }
      const clients = await Client.find(Query).limit(20000);
      return res.json(clients);
    } catch (err) {
      console.log(err);
      res.json({ msg: "algo fue mal", err: err.message });
    }
  }
);

router.post("/totales", async (req, res) => {
  const { url1_w2l, date1, date2, stores, marca } = req.body;

  try {
    let Query = {};

    if (url1_w2l) {
      Object.assign(Query, { url1_w2l: url1_w2l });
    }
    if (date1 && date2) {
      Object.assign(Query, {
        created_at: {
          $gte: new Date(date1 + " 00:00:00"),
          $lt: new Date(date2 + " 23:59:59")
        }
      });
    }

    if (stores) {
      Object.assign(Query, { store: { $in: stores } });
    }

    if (marca) {
      Object.assign(Query, { marca2: marca });
    }

    let nuevos = 0;
    let contactados = 0;
    let gestionados = 0;
    let cotizados = 0;
    let facturados = 0;
    let cancelados = 0;

    const clients = await Client.find(Query).sort({ created_at: -1 });
    clients.forEach(c => {
      if (c.estado === "Nuevo") nuevos++;
      if (c.estado === "Contactado") contactados++;
      if (c.estado === "Gestionado") gestionados++;
      if (c.estado === "Cotizado") cotizados++;
      if (c.estado === "Facturado") facturados++;
      if (c.estado === "Cancelado") cancelados++;
    });

    const post = {
      total_leads: clients.length,
      total_nuevos: nuevos,
      total_contactados: contactados,
      total_cotizados: cotizados,
      total_gestionados: gestionados,
      total_facturados: facturados,
      total_cancelados: cancelados
    };

    res.json(post);
  } catch (err) {
    console.log(err);
    res.json({ msg: "algo fue mal", err });
  }
});

router.post("/leadsWithOutRepetition", async (req, res) => {
  const { date1, date2 } = req.body;
  try {
    let Query = {
      created_at: {
        $gte: new Date(date1 + " 00:00:00"),
        $lt: new Date(date2 + " 23:59:59")
      }
    };

    const clients = await Client.aggregate([
      { $match: Query },
      {
        $group: {
          _id: {
            tipo_documento: "$tipo_documento",
            rut_w2l: "$rut_w2l",
            first_name: "$first_name",
            last_name: "$last_name",
            fone1_w2l: "$fone1_w2l",
            email: "$email",
            url1_w2l: "$url1_w2l",
            url2_w2l: "$url2_w2l",
            brand_w2l: "$brand_w2l",
            marca2: "$marca2",
            model_w2l: "$model_w2l",
            version_w2l: "$version_w2l",
            sap_w2l: "$sap_w2l",
            store: "$store",
            price_w2l: "$price_w2l",
            state: "$state",
            local_w2l: "$local_w2l",
            direccion: "$direccion"
          },
          count: { $sum: 1 }
        }
      },
      {
        $project: {
          _id: 0,
          tipo_documento: "$_id.tipo_documento",
          rut_w2l: "$_id.rut_w2l",
          first_name: "$_id.first_name",
          last_name: "$_id.last_name",
          fone1_w2l: "$_id.fone1_w2l",
          email: "$_id.email",
          url1_w2l: "$_id.url1_w2l",
          marca2: "$_id.marca2",
          model_w2l: "$_id.model_w2l",
          version_w2l: "$_id.version_w2l",
          sap_w2l: "$_id.sap_w2l",
          store: "$_id.store",
          price_w2l: "$_id.price_w2l",
          local_w2l: "$_id.local_w2l",
          direccion: "$_id.direccion",
          count: "$count"
        }
      }
    ]);
    return res.json(clients);
  } catch (err) {
    console.log(err);
    return res.json({
      status: false,
      error: { name: err.name, message: err.message }
    });
  }
});

//SAVE CLIENT FROM INTERNET TEST FOR SEEKO
router.post("/test", async (req, res) => {
  var token = getToken(req.headers);
  if (token) {
    /* Client.create(req.body, function (err, post) {
      if (err) return next(err)
      res.json(post)
    }) */
    try {
      const tienda = tiendas.find(t => t.codigo == req.body.store);
      if (tienda) {
        const {
          first_name,
          last_name,
          fone1_w2l,
          email,
          marca2,
          model_w2l,
          version_w2l,
          tipo_documento,
          rut_w2l,
          created_at
        } = req.body;

        let dataPost = {
          prospect: {
            status: "new",
            id: uniqueId(), // Falta el id
            requestdate: formatDate() /* "2020-09-03" */,
            vehicle: {
              interest: "buy",
              status: "new",
              make: marca2,
              trim: version_w2l,
              model: model_w2l
            },
            customer: {
              contact: {
                name: [
                  {
                    part: "first",
                    value: first_name
                  },
                  {
                    part: "last",
                    value: last_name
                  }
                ],
                email: email,
                dni: rut_w2l,
                phone: [fone1_w2l]
              },
              comments: "Tipo de Documento: " + tipo_documento,
              origin: "",
              privacy: "si",
              terms: "si"
            },
            vendor: {
              source: "derco",
              id: tienda.codigo,
              name: tienda.nombre
            }
          },
          provider: {
            name: "derco"
          }
        };

        fetch("https://www.answerspip.com/apidms/dms/v1/rest/leads/adfv2", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
            //Authorization: token ? `Bearer ${token}` : '',
          },
          body: JSON.stringify(dataPost)
        })
          .then(response => response.json())
          .then(response => res.json(response))
          .catch(err =>
            res.json({
              status: false,
              msg: "Error al enviar",
              err: err.message
            })
          );
      } else {
        return res.json({ status: false, msg: "No hay tienda" });
      }
    } catch (err) {
      return res.json({ status: false, msg: "Error general", err });
    }
  } else {
    return res.status(403).send({ success: false, msg: "Unauthorized." });
  }
});

formatDate = () => {
  var d = new Date(),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
};

uniqueId = () => {
  let id =
    cryptoRandomString({ length: 10, type: "alphanumeric" }) +
    (+new Date()).toString(36).slice(-5);
  return id;
};

module.exports = router;
