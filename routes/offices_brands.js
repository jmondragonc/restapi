/*eslint-disable */
var express = require('express')
var router = express.Router()
var mongoose = require('mongoose')
var Office = require('../models/Office.js')
var NewOffice = require('../models/newOffice')


/* router.get('/:slug', function (req, res) {
    Office.find({ slug: req.params.slug }, function (err, offices) {
        if (err) return next(err)
        res.json(offices)
    })
}) */


router.get('/:slug', async (req, res) => {
    try {
        const offices = await NewOffice.find();
        let officespost = []
        offices.forEach(office => {
            let flat = office.brands.find(o => o.slug == req.params.slug);
            if (flat){
                officespost.push(office);
            }
        })
        res.json(officespost);
    } catch (err) {
        res.json({ status: false, msg: err.message })
    }
})

module.exports = router
