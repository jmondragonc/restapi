/*eslint-disable */
var express = require('express')
var router = express.Router()
var mongoose = require('mongoose')
var Model = require('../models/Version.js')
var passport = require('passport')

require('../config/passport')(passport)

/* GET ALL MODELS */
router.get('/', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers)
  if (token) {
    Model.find(function (err, models) {
      if (err) return next(err)
      res.json(models)
    })
  } else {
    return res.status(403).send({success: false, msg: 'test.'})
  }
})

/* GET SINGLE MODEL BY ID */
router.get('/:id', function(req, res, next) {
  Model.findById(req.params.id, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET MODELS BY BRAND */
router.get('/:slug', function(req, res, next) {
    Model.find({slug: req.params.slug}, function (err, post) {
      if (err) return next(err)
      res.json(post)
    })
  })

/* GET SINGLE MODEL BY SLUG */
router.get('/slug/:slug', function(req, res, next) {
  Model.find({slug: req.params.slug}, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* SAVE MODEL */
router.post('/', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'derco.com.pe');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    //res.setHeader('Access-Control-Allow-Credentials', true);
  
    Model.create(req.body, function (err, post) {
      if (err) return next(err)
      res.json(post)
    })
    
  })

/* UPDATE MODEL */
router.put('/:id', function(req, res, next) {
  Model.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* DELETE MODEL */
router.delete('/:id', function(req, res, next) {
  Model.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

module.exports = router
