/*eslint-disable */
var express = require('express')
var router = express.Router()
var mongoose = require('mongoose')
var Version = require('../models/Version.js')


router.get('/:model_id', function(req, res) {
    Version.find({model_id: req.params.model_id}, function (err, versions) {
        if (err) return next(err)
        res.json(versions)
    })
})

module.exports = router
